#include "HGTDReadoutSim/HGTDCalculator.h"
#include <algorithm>
#include <math.h>
#include <functional>
#include <iostream>
#include <iterator>

//////////////////////////////////////////////////////////////////
///////////////////// HGTDCalculatorCell /////////////////////////
//////////////////////////////////////////////////////////////////

HGTDCalculatorCell::HGTDCalculatorCell() :

  m_x(0),
  m_y(0),
  m_x_cell(0),
  m_y_cell(0),
  m_granularity(0),
  m_sizex(0),
  m_sizey(0),
  m_sizeidx(0),
  m_sizeidy(0),
  m_cellid(0),
  m_type("standard") {}


HGTDCalculatorCell::HGTDCalculatorCell(Float_t x, Float_t y, int x_cell, int y_cell, Float_t granularity,Float_t sizex, Float_t sizey, int sizeidx, int sizeidy, int cellid) :
  m_x(x),
  m_y(y),
  m_x_cell(x_cell),
  m_y_cell(y_cell),
  m_granularity(granularity),
  m_sizex(sizex),
  m_sizey(sizey),
  m_sizeidx(sizeidx),
  m_sizeidy(sizeidy),
  m_cellid(cellid),
  m_type("standard") {}

HGTDCalculatorCell::HGTDCalculatorCell(Float_t x, Float_t y, int x_cell, int y_cell, Float_t granularity, Float_t sizex, Float_t sizey, int sizeidx, int sizeidy, int cellid, std::string type) :
  m_x(x),
  m_y(y),
  m_x_cell(x_cell),
  m_y_cell(y_cell),
  m_granularity(granularity),
  m_sizex(sizex),
  m_sizey(sizey),
  m_sizeidx(sizeidx),
  m_sizeidy(sizeidy),
  m_cellid(cellid),
  m_type(type) {}

HGTDCalculatorCell::HGTDCalculatorCell( const HGTDCalculatorCell& Cell ):
  m_x(Cell.x()),
  m_y(Cell.y()),
  m_x_cell(Cell.x_cell()),
  m_y_cell(Cell.y_cell()),
  m_granularity(Cell.granularity()),
  m_sizex(Cell.sizex()),
  m_sizey(Cell.sizey()),
  m_sizeidx(Cell.sizeidx()),
  m_sizeidy(Cell.sizeidy()),
  m_cellid(Cell.cellid()),
  m_type(Cell.type()) {}

void HGTDCalculatorCell::operator=(const HGTDCalculatorCell& Cell){
  m_x=Cell.x();
  m_y=Cell.y();
  m_x_cell=Cell.x_cell();
  m_y_cell=Cell.y_cell();
  m_granularity=Cell.granularity();
  m_sizex=Cell.sizex();
  m_sizey=Cell.sizey();
  m_sizeidx=Cell.sizeidx();
  m_sizeidy=Cell.sizeidy();
  m_cellid=Cell.cellid();
  m_type=Cell.type();
}

bool HGTDCalculatorCell::operator<(const HGTDCalculatorCell& Cell)const{
  if( m_x+m_y == Cell.x()+Cell.y())
    return (m_x < Cell.x());

  return ((m_x+m_y) < (Cell.x()+Cell.y()));
}

bool HGTDCalculatorCell::operator==(const HGTDCalculatorCell& Cell)const{

  return ((m_x==Cell.x()) && (m_y==Cell.y()));
}

bool HGTDCalculatorCell::is_connected(Float_t x, Float_t y) const{

  // Parameter :
  Float_t guardring = 0.5;

  Float_t x_rel = fabs(x)-m_x;
  Float_t y_rel = fabs(y)-m_y;

  if(m_type=="horizontal"){
    //Here we implement the geometrie "horizontal" of the Cells
    //This geometrie represent an horizontal strip of cell
    //at the top and the bottom of this strip there is 0.5 mm of dead
    if( (y_rel<=guardring) || (y_rel>=(m_sizey-guardring)) ){
      return false;
    }
    else
      return true;
  }

  if(m_type=="vertical"){
    //Here we implement the geometrie "vertical" of the Cells
    //This geometrie represent an vertical strip of cell
    //at the right and the left of this strip there is 0.5 mm of dead
    if( (x_rel<=guardring) || (x_rel>=(m_sizex-guardring)) ){
      return false;
    }
    else
      return true;
  }

  if(m_type=="granularity"){
    //Here we implement the geometrie "granularity" of the Cells
    //This geometrie represent a cell with no dead zone
    if( (x_rel<=0) || (x_rel>=m_sizex) || (y_rel<=0) || (y_rel>=m_sizey) ){
      return false;
    }
    else
      return true;
  }
  if(m_type=="dead"){
    //Here we implement the geometrie "dead" of the Cells
    //This geometrie represent a dead cell
    return false;
  }
  std::cout << "Type of Cell non valid" << std::endl;
  return false;
}

std::pair<Float_t,Float_t> HGTDCalculatorCell::cell_pos(int x, int y) const{

  // Parameter :
  Float_t x_rel = (abs(x)-1-m_x_cell)*m_granularity;
  Float_t y_rel = (abs(y)-1-m_y_cell)*m_granularity;

  Float_t x_sen = m_granularity/2;
  Float_t y_sen = m_granularity/2;

  Float_t guardring = 0.5;

  if( abs(x)-1<0 || abs(y)-1<0 ){
    return (std::make_pair(0,0));
  }


  if(m_type=="horizontal"){

    x_sen+=x_rel;
    y_sen+=y_rel+guardring;
    if(x_sen <= m_sizex && y_sen <= m_sizey){
      return (std::make_pair(x_sen+m_x,y_sen+m_y));
    }
    else{
      std::cout << "ERROR ID OUTSIDE THE CELL" << std::endl;
      return (std::make_pair(0,0));
    }
  }

  if(m_type=="vertical"){

    x_sen+=x_rel+guardring;
    y_sen+=y_rel;
    if(x_sen <= m_sizex && y_sen <= m_sizey){
      return (std::make_pair(x_sen+m_x,y_sen+m_y));
    }
    else{
      std::cout << "ERROR ID OUTSIDE THE CELL" << std::endl;
      return (std::make_pair(0,0));
    }
  }

  if(m_type=="granularity"){
    return (std::make_pair(0,0));
  }

  if(m_type=="dead"){
    return (std::make_pair(0,0));
  }

  std::cout << "Type of Cell non valid" << std::endl;
  return (std::make_pair(0,0));
}

std::pair<int,int> HGTDCalculatorCell::cell_ID(Float_t x, Float_t y) const{

  // Parameter :
  Float_t x_rel = fabs(x)-m_x;
  Float_t y_rel = fabs(y)-m_y;

  Float_t guardring = 0.5;

  int index_x=0;
  int index_y=0;

  if(m_type=="horizontal"){
    if( (y_rel>=guardring) || (y_rel<=(m_sizey-guardring)) ){
      index_x=1+floor((x_rel)/m_granularity);
      index_y=1+floor((y_rel-guardring)/m_granularity);
      return (std::make_pair(index_x+m_x_cell,index_y+m_y_cell));
    }
    return (std::make_pair(0,0));
  }

  if(m_type=="vertical"){
    if( (x_rel>=guardring) || (x_rel<=(m_sizex-guardring)) ){
      index_x=1+floor((x_rel-guardring)/m_granularity);
      index_y=1+floor((y_rel)/m_granularity);
      return (std::make_pair(index_x+m_x_cell,index_y+m_y_cell));
    }
    return (std::make_pair(0,0));
  }

  if(m_type=="granularity"){
    return (std::make_pair(0,0));
  }

  if(m_type=="dead"){
    return (std::make_pair(0,0));
  }

  std::cout << "Type of Cell non valid" << std::endl;
  return (std::make_pair(0,0));
}

//////////////////////////////////////////////////////////////////
/////////////////////// HGTDCalculator ///////////////////////////
//////////////////////////////////////////////////////////////////

HGTDCalculator::HGTDCalculator(){this->build();}

HGTDCalculator::HGTDCalculator(std::string build_name){

  this->build(build_name);

}

HGTDCalculator::HGTDCalculator( const HGTDCalculator& Calculator ):
  m_rmax(Calculator.rmax()),
  m_ready(Calculator.ready()),
  m_det(Calculator.det()),
  m_build_name(Calculator.build_name()){}

void HGTDCalculator::operator=(const HGTDCalculator& Calculator){
  m_rmax=Calculator.rmax();
  m_ready=Calculator.ready();
  m_det=Calculator.det();
  m_build_name=Calculator.build_name();
}

void HGTDCalculator::build(){
  m_build_name = "standard";
  //Here we use the geometrie "standard" of the Detector
  Float_t x_start[16] = {120, 114, 90, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  Float_t x_end[16] = {644, 638, 632, 632, 632, 614, 596, 578, 560, 524, 506, 470, 416, 362, 308, 218};

  m_det.resize(16);
  Float_t y_pos=1;

  for(int i=0;i<16;i++){
    m_det.at(i)=HGTDCalculatorCell(x_start[i], y_pos, 0, 39*i, 1, x_end[i]-x_start[i], 40, x_end[i]-x_start[i], 39, i, "horizontal");
    y_pos += 40;
  }

  m_ready=true;
  return;

}

void HGTDCalculator::build(std::string build_name){

  m_build_name = build_name;

  if(build_name=="standard"){

    //Here we use the geometrie "standard" of the Detector
    Float_t x_start[16] = {120, 114, 90, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    Float_t x_end[16] = {644, 638, 632, 632, 632, 614, 596, 578, 560, 524, 506, 470, 416, 362, 308, 218};

    m_det.resize(16);
    Float_t y_pos=1;

    for(int i=0;i<16;i++){
      m_det.at(i)=HGTDCalculatorCell(x_start[i], y_pos, 0, 39*i, 1, x_end[i]-x_start[i], 40, x_end[i]-x_start[i], 39, i, "horizontal");
      y_pos += 40;
    }

    m_ready=true;
    return;
  }

  if(build_name=="diagonale"){

    //Here we use the geometrie "standard" of the Detector
    Float_t x_start[23] = {120, 114, 90, 1, 41, 81, 121, 161, 201, 161, 241, 241, 241, 281, 321, 361, 361, 361, 361, 401, 441, 481, 481};
    Float_t x_size[23] = {524, 524, 542, 40, 40, 40, 40, 40, 40, 470, 380, 362, 40, 40, 40, 236, 218, 200, 40, 40, 40, 56, 20};

    Float_t y_start[23] = {1, 41, 81, 120, 114, 121, 121, 161, 161, 121, 161, 201, 241, 241, 241, 241, 281, 321, 361, 361, 361, 361, 401};
    Float_t y_size[23] = {40, 40, 40, 524, 524, 506, 506, 452, 452, 40, 40, 40, 344, 326, 308, 40, 40, 40, 164, 146, 110, 40, 40};

    m_det.resize(23);

    for(int i=0;i<3;i++){
      m_det.at(i)=HGTDCalculatorCell(x_start[i], y_start[i], 117, 39*(i), 1, x_size[i], y_size[i], x_size[i], 39, i, "horizontal");
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    for(int i=3;i<5;i++){
      m_det.at(i)=HGTDCalculatorCell(x_start[i], y_start[i], 39*(i-3), 117, 1, x_size[i], y_size[i], 39, y_size[i], i, "vertical");
    }
    for(int i=5;i<7;i++){
      m_det.at(i)=HGTDCalculatorCell(x_start[i], y_start[i], 78+39*(i-5), 117, 1, x_size[i], y_size[i], 39, y_size[i], i, "vertical");
    }
    for(int i=7;i<9;i++){
      m_det.at(i)=HGTDCalculatorCell(x_start[i], y_start[i], 156+39*(i-7), 156, 1, x_size[i], y_size[i], 39, y_size[i], i, "vertical");
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    m_det.at(9)=HGTDCalculatorCell(x_start[9], y_start[9], 156, 117, 1, x_size[9], y_size[9], x_size[9], 39, 9, "horizontal");

    for(int i=10;i<12;i++){
      m_det.at(i)=HGTDCalculatorCell(x_start[i], y_start[i], 234, 156+39*(i-10), 1, x_size[i], y_size[i], x_size[i], 39, i, "horizontal");
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    for(int i=12;i<15;i++){
      m_det.at(i)=HGTDCalculatorCell(x_start[i], y_start[i], 234+39*(i-12), 234, 1, x_size[i], y_size[i], 39, y_size[i], i, "vertical");
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    for(int i=15;i<18;i++){
      m_det.at(i)=HGTDCalculatorCell(x_start[i], y_start[i], 351, 234+39*(i-15), 1, x_size[i], y_size[i], x_size[i], 39, i, "horizontal");
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    for(int i=18;i<21;i++){
      m_det.at(i)=HGTDCalculatorCell(x_start[i], y_start[i], 351+39*(i-18), 351, 1, x_size[i], y_size[i], 39, y_size[i], i, "vertical");
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    for(int i=21;i<23;i++){
      m_det.at(i)=HGTDCalculatorCell(x_start[i], y_start[i], 468, 351+39*(i-21), 1, x_size[i], y_size[i], x_size[i], 39, i, "horizontal");
    }

    m_ready=true;
    return;
  }

  if(build_name=="Helix_0"){

    //Here we use the geometrie "Helix_0" of the Detector
    Float_t x_start[16] = { 1, 41, 81, 121, 161, 201, 241, 281, 321, 361, 401, 441, 481, 521, 561, 601};
    Float_t x_size[16] = { 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40};

    Float_t y_start[16] = {120, 113, 89, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
    Float_t y_size[16] = {520, 526, 547, 628, 619, 607, 592, 574, 553, 528, 498, 463, 422, 371, 307, 219};

    m_det.resize(16);

    for(int i=0;i<3;i++){
      m_det.at(i)=HGTDCalculatorCell(x_start[i], y_start[i], 39*i, 117, 1, x_size[i], y_size[i], 39, y_size[i], i, "vertical");
    }

    for(int i=3;i<16;i++){
      m_det.at(i)=HGTDCalculatorCell(x_start[i], y_start[i], 39*i, 0, 1, x_size[i], y_size[i], 39, y_size[i], i, "vertical");
    }

    m_ready=true;
    return;
  }

  if(build_name=="Helix_1"){

    //Here we use the geometrie "Helix_1" of the Detector
    Float_t x_start[17] = { 1, 41, 81, 120, 121, 161, 201, 241, 281, 321, 361, 401, 441, 481, 521, 561, 601};
    Float_t x_size[17] = { 40, 40, 40, 520, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40};

    Float_t y_start[17] = {120, 113, 89, 1, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41};
    Float_t y_size[17] = {520, 526, 547, 40, 588, 579, 567, 552, 534, 513, 488, 458, 423, 382, 331, 267, 199};

    m_det.resize(17);

    for(int i=0;i<3;i++){
      m_det.at(i)=HGTDCalculatorCell(x_start[i], y_start[i], 39*i, 117, 1, x_size[i], y_size[i], 39, y_size[i], i, "vertical");
    }
    m_det.at(3)=HGTDCalculatorCell(x_start[3], y_start[3], 117, 0, 1, x_size[3], y_size[3], x_size[3], 39, 3, "horizontal");

    for(int i=4;i<17;i++){
      m_det.at(i)=HGTDCalculatorCell(x_start[i], y_start[i], 39*(i-1), 39, 1, x_size[i], y_size[i], 39, y_size[i], i, "vertical");
    }

    m_ready=true;
    return;
  }

  if(build_name=="Helix_2"){

    //Here we use the geometrie "Helix_2" of the Detector
    Float_t x_start[18] = { 1, 41, 81, 120, 113, 121, 161, 201, 241, 281, 321, 361, 401, 441, 481, 521, 561, 601};
    Float_t x_size[18] = { 40, 40, 40, 524, 524, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40};

    Float_t y_start[18] = {120, 113, 89, 1, 41, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81};
    Float_t y_size[18] = {524, 524, 548, 40, 40, 542, 542, 530, 506, 488, 470, 452, 416, 380, 344, 290, 218, 128};

    m_det.resize(18);

    for(int i=0;i<3;i++){
      m_det.at(i)=HGTDCalculatorCell(x_start[i], y_start[i], 39*i, 117, 1, x_size[i], y_size[i], 39, y_size[i], i, "vertical");
    }

    for(int i=3;i<5;i++){
      m_det.at(i)=HGTDCalculatorCell(x_start[i], y_start[i], 117, 39*(i-3), 1, x_size[i], y_size[i], x_size[i], 39, i, "horizontal");
    }

    for(int i=5;i<18;i++){
      m_det.at(i)=HGTDCalculatorCell(x_start[i], y_start[i], 39*(i-2), 78, 1, x_size[i], y_size[i], 39, y_size[i], i, "vertical");
    }

    m_ready=true;
    return;
  }

  if(build_name=="Helix_0_l2"){

    //Here we use the geometrie "Helix_0" for the layer 2 of the Detector
    Float_t x_start[16] = {120, 113, 89, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
    Float_t x_size[16] = {520, 526, 547, 628, 619, 607, 592, 574, 553, 528, 498, 463, 422, 371, 307, 219};

    Float_t y_start[16] = { 1, 41, 81, 121, 161, 201, 241, 281, 321, 361, 401, 441, 481, 521, 561, 601};
    Float_t y_size[16] = { 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40};


    m_det.resize(16);

    for(int i=0;i<3;i++){
      m_det.at(i)=HGTDCalculatorCell(x_start[i], y_start[i], 117, 39*i, 1, x_size[i], y_size[i], x_size[i], 39, i, "horizontal");
    }

    for(int i=3;i<16;i++){
      m_det.at(i)=HGTDCalculatorCell(x_start[i], y_start[i], 0, 39*i, 1, x_size[i], y_size[i], x_size[i], 39, i, "horizontal");
    }

    m_ready=true;
    return;
  }

  if(build_name=="Helix_1_l2"){

    //Here we use the geometrie "Helix_1" for the layer 2 of the Detector
    Float_t x_start[17] = {120, 113, 89, 1, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41, 41};
    Float_t x_size[17] = {520, 526, 547, 40, 588, 579, 567, 552, 534, 513, 488, 458, 423, 382, 331, 267, 199};

    Float_t y_start[17] = { 1, 41, 81, 120, 121, 161, 201, 241, 281, 321, 361, 401, 441, 481, 521, 561, 601};
    Float_t y_size[17] = { 40, 40, 40, 520, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40};

    m_det.resize(17);

    for(int i=0;i<3;i++){
      m_det.at(i)=HGTDCalculatorCell(x_start[i], y_start[i], 117, 39*i, 1, x_size[i], y_size[i], x_size[i], 39, i, "horizontal");
    }
    m_det.at(3)=HGTDCalculatorCell(x_start[3], y_start[3], 0, 117, 1, x_size[3], y_size[3], 39, y_size[3], 3, "vertical");

    for(int i=4;i<17;i++){
      m_det.at(i)=HGTDCalculatorCell(x_start[i], y_start[i], 39, 39*(i-1), 1, x_size[i], y_size[i], x_size[i], 39, i, "horizontal");
    }

    m_ready=true;
    return;
  }

  if(build_name=="Helix_2_l2"){

    //Here we use the geometrie "Helix_2" for the Layer 2 of the Detector
    Float_t x_start[18] = {120, 113, 89, 1, 41, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81, 81};
    Float_t x_size[18] = {524, 524, 548, 40, 40, 542, 542, 530, 506, 488, 470, 452, 416, 380, 344, 290, 218, 128};

    Float_t y_start[18] = { 1, 41, 81, 120, 113, 121, 161, 201, 241, 281, 321, 361, 401, 441, 481, 521, 561, 601};
    Float_t y_size[18] = { 40, 40, 40, 524, 524, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40, 40};




    m_det.resize(18);

    for(int i=0;i<3;i++){
      m_det.at(i)=HGTDCalculatorCell(x_start[i], y_start[i], 117, 39*i, 1, x_size[i], y_size[i], x_size[i], 39, i, "horizontal");
    }

    for(int i=3;i<5;i++){
      m_det.at(i)=HGTDCalculatorCell(x_start[i], y_start[i], 39*(i-3), 117, 1, x_size[i], y_size[i], 39, y_size[i], i, "vertical");
    }

    for(int i=5;i<18;i++){
      m_det.at(i)=HGTDCalculatorCell(x_start[i], y_start[i], 78, 39*(i-2), 1, x_size[i], y_size[i], x_size[i], 39, i, "horizontal");
    }

    m_ready=true;
    return;
  }

  std::cout << "invalide buid name : " << build_name  << std::endl;
}

HGTDCalculatorCell HGTDCalculator::CellXY(Float_t x,Float_t y) const {

  unsigned int i=0;
  Float_t absx=fabs(x);
  Float_t absy=fabs(y);
  if(m_ready){
    for(i=0;i<m_det.size();i++){
      if( (m_det[i].y()<=absy && absy<m_det[i].y()+m_det[i].sizey()) && (m_det[i].x()<=absx && absx<m_det[i].x()+m_det[i].sizex())){
        return m_det[i];
      }
    }
  }
  else{
    std::cout << "Detector not ready" << std::endl;
    return HGTDCalculatorCell(0, 0, 0, 0, 0, 0, 0, 0, 0, 0,"dead");
  }

  return HGTDCalculatorCell(0, 0, 0, 0, 0, 0, 0, 0, 0, 0,"dead");
}


HGTDCalculatorCell HGTDCalculator::CellID(int x,int y) const {

  unsigned int i=0;
  int absx=abs(x)-1;
  int absy=abs(y)-1;
  if(m_ready){
    for(i=0;i<m_det.size();i++){
      if(m_det[i].y_cell()<=absy && absy<m_det[i].y_cell()+m_det[i].sizeidy() && m_det[i].x_cell()<=absx && absx<m_det[i].x_cell()+m_det[i].sizeidx()){
        return m_det[i];
      }
    }
  }
  else{
    std::cout << "Detector not ready" << std::endl;
    return HGTDCalculatorCell(0, 0, 0, 0, 0, 0, 0, 0, 0, 0,"dead");
  }

  return HGTDCalculatorCell(0, 0, 0, 0, 0, 0, 0, 0, 0, 0,"dead");

}

bool HGTDCalculator::is_connected(Float_t x, Float_t y, std::string side) const {
  if(m_ready){
    this->symmetry(x,y,0);
    HGTDCalculatorCell Cell = this->CellXY(x,y);
    if(Cell.is_connected(x,y)){
      if(side=="None"){
        return true;
      }
      int start=0;
      int pos=0;
      int mod_80=0;
      double length_R320=0;

      if(Cell.type()=="vertical"){
        start = Cell.y();
        pos = y - start;
        if( 320*320 - Cell.x()*Cell.x() > 0 ){
          length_R320 = sqrt(320*320 - Cell.x()*Cell.x());
          mod_80 = round( ( ceil( ( length_R320-start-2 )/24 )*24+2 )/24 );
        }
      }

      else if(Cell.type()=="horizontal"){
        start = Cell.x();
        pos = x - start;
        if( 320*320 - Cell.y()*Cell.y()>0 ){
          length_R320 = sqrt(320*320 - Cell.y()*Cell.y());
          mod_80 = round( ( ceil( ( length_R320-start-2 )/24 )*24+2 )/24 );
        }
      }

      if(side=="Top"){
        if(Cell.cellid() < 10){
          if(pos < mod_80*24 ){
            if(pos%24<20 && pos >= 0){
              return true;
            }
          }
          else{
            if( (pos-mod_80*24)%36 < 20 && pos-mod_80*24 >= 0 ){
              return true;
            }
          }
        }
        else{
          if( pos%36<20 && pos >= 0 ){
            return true;
          }
        }
        return false;
      }

      else if(side=="Bottom"){
        if(Cell.cellid() < 10){
          if( (pos-6) < (mod_80-1)*24 + 20 ){
            if( (pos-6)%24<20 && (pos-6) >= 0 ){
              return true;
            }
          }
          else{
            if( (pos-6-(mod_80-1)*24-20)%36 >= 16 && (pos-6-(mod_80-1)*24-20) >= 0 ){
              return true;
            }
          }
        }
        else{
          if( (pos-10)%36 < 20 && (pos-10) >= 0){
            return true;
          }
        }
        return false;
      }
      else{
        std::cout << side << " is not a valid side" << std::endl;
        return false;
      }
    }
    else{
      return false;
    }
  }
  std::cout << "Detector not ready" << std::endl;
  return false;
}


void HGTDCalculator::id_to_pos(int ix, int iy, Float_t& px, Float_t& py){
  int quad = this->symmetry(ix,iy,0);
  HGTDCalculatorCell Cell = this->CellID(ix,iy);
  px = Cell.cell_pos(ix,iy).first;
  py = Cell.cell_pos(ix,iy).second;
  this->symmetry(px,py,quad);
  return;
}

void HGTDCalculator::pos_to_id(Float_t px, Float_t py, int& ix, int& iy){
  int quad = this->symmetry(px,py,0);
  HGTDCalculatorCell Cell = this->CellXY(px,py);
  ix = Cell.cell_ID(px,py).first;
  iy = Cell.cell_ID(px,py).second;
  this->symmetry(ix,iy,quad);


  return ;
}

void HGTDCalculator::pos_Cell(Float_t px0, Float_t py0, Float_t& px, Float_t& py){
  int quad = this->symmetry(px0,py0,0);
  HGTDCalculatorCell Cell = this->CellXY(px0,py0);
  int ix=0;
  int iy=0;
  ix = Cell.cell_ID(px0,py0).first;
  iy = Cell.cell_ID(px0,py0).second;
  px = Cell.cell_pos(ix,iy).first;
  py = Cell.cell_pos(ix,iy).second;
  this->symmetry(px,py,quad);
  return ;
}



//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
