#include "HGTDReadoutSim/HGTDTimingResolution.h"

#include <math.h>
#include <iostream>

HGTDTimingResolution::HGTDTimingResolution()
{
  m_sigmaType = HGTDTimingEnum::HGTDInitialResolution;
  m_Rmin      = 122.;
  m_Rmax      = 640.;
  
  double zposition = 3500.;
  Init(zposition);
}
HGTDTimingResolution::HGTDTimingResolution(unsigned int sigmaType, double Rmin, double Rmax, double Zposition)
{
  m_sigmaType = sigmaType;
  m_Rmin = Rmin;
  m_Rmax = Rmax;

  Init(Zposition);
}
void HGTDTimingResolution::Init(double zposition){
  m_version = "HGTD Timing v-beta";

  m_Zposition= zposition;

  m_finalResolution.push_back(0.);
  m_finalResolution.push_back(51);
  m_finalResolution.push_back(49);
  m_finalResolution.push_back(44);
  m_finalResolution.push_back(41.2);
  m_finalResolution.push_back(37.3);
  m_finalResolution.push_back(35.5);
  m_finalResolution.push_back(33.6);

  m_intermediateResolution.push_back(0);
  m_intermediateResolution.push_back(47.1);
  m_intermediateResolution.push_back(39.7);
  m_intermediateResolution.push_back(35.5);
  m_intermediateResolution.push_back(33.6);
  m_intermediateResolution.push_back(31.1);
  m_intermediateResolution.push_back(30.1);
  m_intermediateResolution.push_back(29.0);

  m_electronicsResolution = 25.;
}

// get the timing resolution for a hit at a radius R (unit: mm)
double HGTDTimingResolution::TimingResolutionPerHit4R(double requestedRadius){

  if ( !RInRange(requestedRadius) ) return 99999.;

  double sigmaT = 9999.;

  switch ( m_sigmaType ) {
  case HGTDTimingEnum::HGTDInitialResolution:
    sigmaT = 30.;
    break;
  case HGTDTimingEnum::HGTDFinalResolution:
  case HGTDTimingEnum::HGTDIntermediateResolution:
    sigmaT = Resolution(fabs(requestedRadius));
    break;
  }

  // include the effect of electronics as 25ps added in quadrature
  sigmaT = sigmaT*sigmaT + m_electronicsResolution*m_electronicsResolution;
  sigmaT = sqrt(sigmaT);

  return sigmaT;
}

// get the timing resolution for a Track at eta
double HGTDTimingResolution::TimingResolutionPerTrack4Eta(double requestedEta){

  if ( !EtaInRange(requestedEta) ) return 99999.;

  double requestedRadius = TranslateEta2R(requestedEta);

  double sigmaT = TimingResolutionPerTrack4R(requestedRadius);

  return sigmaT;
}

// get the timing resolution for a Track at a radius R (unit: mm)
double HGTDTimingResolution::TimingResolutionPerTrack4R(double requestedRadius){

  if ( !RInRange(requestedRadius) ) return 99999.;

  double nbOfHits = AverageNhitsPerTrack4R(requestedRadius);
  if ( nbOfHits == 0. ) return 9999.;

  double sigmaT = TimingResolutionPerHit4R(requestedRadius)/sqrt(nbOfHits);

  return sigmaT;
}

// get the correct number of hits (it's a double since it's an average)
// R in mm
double HGTDTimingResolution::AverageNhitsPerTrack4R(double requestedRadius){

  if ( !RInRange(requestedRadius) ) return 0.;

  double averageNbOfHits = 0.;
  if ( fabs(requestedRadius) >= m_Rmin     && fabs(requestedRadius) < 134.8 )
    averageNbOfHits = 2.66;
  else if ( fabs(requestedRadius) >= 134.8 && fabs(requestedRadius) < 149.  )
    averageNbOfHits = 2.63;
  else if ( fabs(requestedRadius) >= 149.  && fabs(requestedRadius) < 164.7 )
    averageNbOfHits = 2.94;
  else if ( fabs(requestedRadius) >= 164.7 && fabs(requestedRadius) < 182.1 )
    averageNbOfHits = 2.79;
  else if ( fabs(requestedRadius) >= 182.1 && fabs(requestedRadius) < 201.2 )
    averageNbOfHits = 2.79;
  else if ( fabs(requestedRadius) >= 201.2 && fabs(requestedRadius) < 222.4 )
    averageNbOfHits = 2.83;
  else if ( fabs(requestedRadius) >= 222.4 && fabs(requestedRadius) < 245.9 )
    averageNbOfHits = 2.89;
  else if ( fabs(requestedRadius) >= 245.9 && fabs(requestedRadius) < 271.8 )
    averageNbOfHits = 2.89;
  else if ( fabs(requestedRadius) >= 271.8 && fabs(requestedRadius) < 316.  )
    averageNbOfHits = 2.93;
  else if ( fabs(requestedRadius) >= 316.  && fabs(requestedRadius) < 427.3 )
    averageNbOfHits = 1.96;
  else if ( fabs(requestedRadius) >= 427.3  && fabs(requestedRadius) < m_Rmax )
    averageNbOfHits = 1.96;

  return averageNbOfHits;
}

// check if the request is in range:
bool HGTDTimingResolution::changeResolution2(unsigned int sigmaType){

  switch (sigmaType){
  case HGTDTimingEnum::HGTDInitialResolution:
  case HGTDTimingEnum::HGTDFinalResolution:
  case HGTDTimingEnum::HGTDIntermediateResolution:
    m_sigmaType = sigmaType;
    break;
  default:
    return false;
  }
  
  return true;
}

// returns the per Hit resolution for the final performance
double HGTDTimingResolution::Resolution(double radius){
  double resolution = 9999.;

  unsigned int index = (int) (radius+49.999)/100;

  switch ( m_sigmaType ){
  case HGTDTimingEnum::HGTDFinalResolution:
    if ( index < m_finalResolution.size() ) 
      resolution =  m_finalResolution[index];
    break;
  case HGTDTimingEnum::HGTDIntermediateResolution:
    if ( index < m_intermediateResolution.size() ) 
      resolution =  m_intermediateResolution[index];
    break;
  }

  return resolution;
}
double HGTDTimingResolution::ElectronicsResolution(){
   return m_electronicsResolution;
}

// check if the request is in range:
bool HGTDTimingResolution::RInRange(double requestedRadius){

  if ( fabs(requestedRadius) >= m_Rmin && fabs(requestedRadius) <= m_Rmax ) 
    return true;

  return false;
}
// check if the request is in range:
bool HGTDTimingResolution::EtaInRange(double requestedEta){

  double requestedRadius = TranslateEta2R(requestedEta);
  bool inRange = RInRange(requestedRadius);
  return inRange;
}
// translate radius to eta
double HGTDTimingResolution::TranslateR2Eta(double requestedRadius){

  double requestedEta = fabs(-log(tan(atan(fabs(requestedRadius)/m_Zposition)/2.)));
  return requestedEta;
}
// translate eta to radius
double HGTDTimingResolution::TranslateEta2R(double requestedEta){

  double requestedRadius =  fabs(tan(atan(exp(-requestedEta))*2.)*m_Zposition);
  return requestedRadius;
}

void HGTDTimingResolution::Print(){

  std::cout << "HGTDTimingResolution version: " << m_version << std::endl;
  std::cout << "Type "  << m_sigmaType << std::endl;
  std::cout << "Rmin= " <<  m_Rmin  << std::endl;
  std::cout << "Rmax= " <<  m_Rmax  << std::endl;
  std::cout << "Z=    " <<  m_Zposition << std::endl;

  std::cout << std::endl;
  for (unsigned int i=0; i< m_intermediateResolution.size(); i++){
    std::cout << "Resolution r=" << i*100 << " mm " << TimingResolutionPerHit4R(i*100) << "ps" << std::endl;
  }

}