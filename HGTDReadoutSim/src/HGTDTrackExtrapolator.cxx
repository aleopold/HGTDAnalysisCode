#include "HGTDReadoutSim/HGTDTrackExtrapolator.h"

#include "TrkExInterfaces/IExtrapolator.h"
#include "CaloTrackingGeometry/ICaloSurfaceBuilder.h"
#include "TrkSurfaces/DiscSurface.h"


HGTDTrackExtrapolator::HGTDTrackExtrapolator(){

  Amg::Transform3D* translateAlongPositiveZ1_top = new Amg::Transform3D(Amg::Vector3D(0.,0.,3516.93 ));
  m_discSurface_atCaloEntrance_positiveZ1_top = new Trk::DiscSurface(translateAlongPositiveZ1_top, 0., 10000.);

  Amg::Transform3D* translateAlongNegativeZ1_top = new Amg::Transform3D(Amg::Vector3D(0.,0.,-3516.93 ));
  m_discSurface_atCaloEntrance_negativeZ1_top = new Trk::DiscSurface(translateAlongNegativeZ1_top, 0., 10000.);

  Amg::Transform3D* translateAlongPositiveZ1_bottom = new Amg::Transform3D(Amg::Vector3D(0.,0.,3525.12 ));
  m_discSurface_atCaloEntrance_positiveZ1_bottom = new Trk::DiscSurface(translateAlongPositiveZ1_bottom, 0., 10000.);

  Amg::Transform3D* translateAlongNegativeZ1_bottom = new Amg::Transform3D(Amg::Vector3D(0.,0.,-3525.12 ));
  m_discSurface_atCaloEntrance_negativeZ1_bottom = new Trk::DiscSurface(translateAlongNegativeZ1_bottom, 0., 10000.);

  Amg::Transform3D* translateAlongPositiveZ2_top = new Amg::Transform3D(Amg::Vector3D(0.,0.,3533.33 ));
  m_discSurface_atCaloEntrance_positiveZ2_top = new Trk::DiscSurface(translateAlongPositiveZ2_top, 0., 10000.);

  Amg::Transform3D* translateAlongNegativeZ2_top = new Amg::Transform3D(Amg::Vector3D(0.,0.,-3533.33 ));
  m_discSurface_atCaloEntrance_negativeZ2_top = new Trk::DiscSurface(translateAlongNegativeZ2_top, 0., 10000.);

  Amg::Transform3D* translateAlongPositiveZ2_bottom = new Amg::Transform3D(Amg::Vector3D(0.,0.,3541.52 ));
  m_discSurface_atCaloEntrance_positiveZ2_bottom = new Trk::DiscSurface(translateAlongPositiveZ2_bottom, 0., 10000.);

  Amg::Transform3D* translateAlongNegativeZ2_bottom = new Amg::Transform3D(Amg::Vector3D(0.,0.,-3541.52 ));
  m_discSurface_atCaloEntrance_negativeZ2_bottom = new Trk::DiscSurface(translateAlongNegativeZ2_bottom, 0., 10000.);

}


HGTDTrackExtrapolator::HGTDTrackExtrapolator(ToolHandle<Trk::IExtrapolator> * extrapolator) :
  m_extrapolator(extrapolator){

    Amg::Transform3D* translateAlongPositiveZ1_top = new Amg::Transform3D(Amg::Vector3D(0.,0.,3516.93 ));
    m_discSurface_atCaloEntrance_positiveZ1_top = new Trk::DiscSurface(translateAlongPositiveZ1_top, 0., 10000.);

    Amg::Transform3D* translateAlongNegativeZ1_top = new Amg::Transform3D(Amg::Vector3D(0.,0.,-3516.93 ));
    m_discSurface_atCaloEntrance_negativeZ1_top = new Trk::DiscSurface(translateAlongNegativeZ1_top, 0., 10000.);

    Amg::Transform3D* translateAlongPositiveZ1_bottom = new Amg::Transform3D(Amg::Vector3D(0.,0.,3525.12 ));
    m_discSurface_atCaloEntrance_positiveZ1_bottom = new Trk::DiscSurface(translateAlongPositiveZ1_bottom, 0., 10000.);

    Amg::Transform3D* translateAlongNegativeZ1_bottom = new Amg::Transform3D(Amg::Vector3D(0.,0.,-3525.12 ));
    m_discSurface_atCaloEntrance_negativeZ1_bottom = new Trk::DiscSurface(translateAlongNegativeZ1_bottom, 0., 10000.);

    Amg::Transform3D* translateAlongPositiveZ2_top = new Amg::Transform3D(Amg::Vector3D(0.,0.,3533.33 ));
    m_discSurface_atCaloEntrance_positiveZ2_top = new Trk::DiscSurface(translateAlongPositiveZ2_top, 0., 10000.);

    Amg::Transform3D* translateAlongNegativeZ2_top = new Amg::Transform3D(Amg::Vector3D(0.,0.,-3533.33 ));
    m_discSurface_atCaloEntrance_negativeZ2_top = new Trk::DiscSurface(translateAlongNegativeZ2_top, 0., 10000.);

    Amg::Transform3D* translateAlongPositiveZ2_bottom = new Amg::Transform3D(Amg::Vector3D(0.,0.,3541.52 ));
    m_discSurface_atCaloEntrance_positiveZ2_bottom = new Trk::DiscSurface(translateAlongPositiveZ2_bottom, 0., 10000.);

    Amg::Transform3D* translateAlongNegativeZ2_bottom = new Amg::Transform3D(Amg::Vector3D(0.,0.,-3541.52 ));
    m_discSurface_atCaloEntrance_negativeZ2_bottom = new Trk::DiscSurface(translateAlongNegativeZ2_bottom, 0., 10000.);

}


void HGTDTrackExtrapolator::SetExtrapolator(ToolHandle<Trk::IExtrapolator> * extrapolator) {
  m_extrapolator = extrapolator;
}

bool HGTDTrackExtrapolator::Track_Extrapolation(const xAOD::TrackParticle* trackParticle, TVector3 & v, TVector3 & vp, int surface){

  const Trk::TrackParameters* trackParameters_atCaloEntrance = 0;
  Trk::ParticleHypothesis particle = Trk::pion;

  switch ( surface ){

  case 0:

    if(trackParticle->eta()>0){
      trackParameters_atCaloEntrance = dynamic_cast< const Trk::TrackParameters * > ((*m_extrapolator)->extrapolate(*trackParticle, *m_discSurface_atCaloEntrance_positiveZ1_top, Trk::alongMomentum, true, particle));
    }

    if(trackParticle->eta()<0){
      trackParameters_atCaloEntrance = dynamic_cast< const Trk::TrackParameters * > ((*m_extrapolator)->extrapolate(*trackParticle, *m_discSurface_atCaloEntrance_negativeZ1_top, Trk::alongMomentum, true, particle));
    }

    break;

  case 2:

    if(trackParticle->eta()>0){
      trackParameters_atCaloEntrance = dynamic_cast< const Trk::TrackParameters * > ((*m_extrapolator)->extrapolate(*trackParticle, *m_discSurface_atCaloEntrance_positiveZ2_top, Trk:: alongMomentum, true, particle));
    }

    if(trackParticle->eta()<0){
      trackParameters_atCaloEntrance = dynamic_cast< const Trk::TrackParameters * > ((*m_extrapolator)->extrapolate(*trackParticle, *m_discSurface_atCaloEntrance_negativeZ2_top, Trk::alongMomentum, true, particle));
    }

    break;

  case 1:

    if(trackParticle->eta()>0){
      trackParameters_atCaloEntrance = dynamic_cast< const Trk::TrackParameters * > ((*m_extrapolator)->extrapolate(*trackParticle, *m_discSurface_atCaloEntrance_positiveZ1_bottom, Trk::alongMomentum, true, particle));
    }

    if(trackParticle->eta()<0){
      trackParameters_atCaloEntrance = dynamic_cast< const Trk::TrackParameters * > ((*m_extrapolator)->extrapolate(*trackParticle, *m_discSurface_atCaloEntrance_negativeZ1_bottom, Trk::alongMomentum, true, particle));
    }

    break;

  case 3:

    if(trackParticle->eta()>0){
      trackParameters_atCaloEntrance = dynamic_cast< const Trk::TrackParameters * > ((*m_extrapolator)->extrapolate(*trackParticle, *m_discSurface_atCaloEntrance_positiveZ2_bottom, Trk::alongMomentum, true, particle));
    }

    if(trackParticle->eta()<0){
      trackParameters_atCaloEntrance = dynamic_cast< const Trk::TrackParameters * > ((*m_extrapolator)->extrapolate(*trackParticle, *m_discSurface_atCaloEntrance_negativeZ2_bottom, Trk::alongMomentum, true, particle));
    }

    break;

  }

  if (trackParameters_atCaloEntrance!=0) {
    double x = trackParameters_atCaloEntrance->position().x();
    double y = trackParameters_atCaloEntrance->position().y();
    double z = trackParameters_atCaloEntrance->position().z();

    double px = trackParameters_atCaloEntrance->momentum().x();
    double py = trackParameters_atCaloEntrance->momentum().y();
    double pz = trackParameters_atCaloEntrance->momentum().z();

    v.SetXYZ(x,y,z);
    vp.SetXYZ(px,py,pz);
    delete trackParameters_atCaloEntrance;

    return(true);
  }
  delete trackParameters_atCaloEntrance;

  return(false);
}
