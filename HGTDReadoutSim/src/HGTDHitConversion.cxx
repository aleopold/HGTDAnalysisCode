#include "HGTDReadoutSim/HGTDHitConversion.h"

// Section of includes for LAr calo tests
#include "GeoAdaptors/GeoLArHit.h" // TODO: remove? /CO

#include "CaloIdentifier/HGTD_ID.h"

// implementation of the dead spaces
#include "HGTDReadoutSim/HGTDCalculator.h"

#include "TH1.h"
#include "TString.h"
#include "TMath.h"

// for the collection
#include <algorithm>
#include <math.h>
#include <functional>
#include <iostream>


HGTDHitConversion::HGTDHitConversion() :
m_granu(true),
m_doPulseAna(true),
m_overlap(true),
m_timeBinSize(0.2) {
  m_Detector = HGTDCalculator();
  m_felec1 = 0.025;
}

HGTDHitConversion::HGTDHitConversion(bool j,bool k, bool l) :
m_granu(j),
m_doPulseAna(k),
m_overlap(l),
m_timeBinSize(0.2) {
  m_Detector = HGTDCalculator();
  m_felec1 = 0.025;
}

HGTDHitConversion::HGTDHitConversion(bool j,bool k, bool l, std::string geo,float timeBinSize) :
m_granu(j),
m_doPulseAna(k),
m_overlap(l),
m_timeBinSize(timeBinSize) {
  m_Detector = HGTDCalculator(geo);
  m_felec1 = 0.025;
}

void HGTDHitConversion::SetResolution(HGTDTimingResolution * Timing) {

  m_Timing = Timing;
  m_felec1 = 0.001*m_Timing->ElectronicsResolution();
}

void HGTDHitConversion::SetcellReductionFactor(int i) {
  m_cellReductionFactor = i;
}

void HGTDHitConversion::Setcfd(double f){
  if(f>0 && f<1)
  m_cfd=f;
  else
  std::cout << "The cfd fraction should be between 0 and 1" << std::endl;
}

void HGTDHitConversion::PulseSimulation(double R){

  double res = m_Timing->TimingResolutionPerHit4R(R);
  res = sqrt(res*res - m_Timing->ElectronicsResolution()*m_Timing->ElectronicsResolution());
  double PulseParameter[4] = {0};
  PulseParameter[0] = 0.036; // Width (scale) parameter of Landau density
  PulseParameter[1] = 5.754; // Most Probable (MP, location) parameter of Landau density
  PulseParameter[3] = m_rand->Gaus(0.318,res*0.001-res*0.0001); // Width (sigma) of convoluted Gaussian function
  PulseParameter[2] = 0.4785+1.4196*PulseParameter[3]; // Total area (integral -inf to inf, normalization constant)

  // Convoluted Landau and Gaussian Fitting Function
  // Adapted to simulate a pulse in an HGTD pad

  // Numeric constants
  double invsq2pi = 0.3989422804014;   // (2 pi)^(-1/2)
  double mpshift  = -0.22278298;       // Landau maximum location

  // Variables
  double mpc = PulseParameter[1] - mpshift * PulseParameter[0];
  double step=0.03;
  double step_par=step/(sqrt(2)*PulseParameter[3]);
  double f_weight = PulseParameter[2] * step * invsq2pi / PulseParameter[3] ;
  double fland;
  double p_time;

  // Convolution integral of Landau and Gaussian by sum
  for(int i=412; i<527; i++) {

    fland = TMath::Landau(3.5+(i*0.005),mpc,PulseParameter[0]) / PulseParameter[0];
    p_time = (1.5 + (i%6-i)*0.005)/ (sqrt(2)*PulseParameter[3]) ;

    for(int point=i%6;point<400;point+=6){

      p_time += step_par;
      m_IPulse[point] += fland * exp16(-p_time*p_time);

    }
  }
  for(int point=0;point<400;point++){

    m_IPulse[point] = f_weight * m_IPulse[point];

  }
}

void HGTDHitConversion::PulseShape(std::map<int,std::pair<double,double> > &Pulsebin, const double t, const double E, double *max) {

  int timebin = 0;
  double energy=0;
  double time =0;

  for(int point=0;point<400;point++){

    energy=m_IPulse[point]*E + m_rand->Gaus(0,0.0006);
    time=(t+point*0.005);
    timebin = (int)(t/0.005)+point;

    auto pulse = Pulsebin.find( timebin );
    if (pulse == Pulsebin.end() ){
      Pulsebin.insert( std::make_pair( timebin , std::make_pair(time*energy,energy )));
      pulse = Pulsebin.find( timebin );
    }
    else{
      (*pulse).second = std::make_pair((*pulse).second.first+time*energy, (*pulse).second.second+energy );
    }
    if(max[0]<(*pulse).second.second){
      max[0]=(*pulse).second.second;
      max[1]=(*pulse).second.first/max[0];
    }
  }
  return ;
}

void HGTDHitConversion::TimeBin(LArHit * hi, const HGTD_ID * m_hgtdID) {

  Float_t zposneg  = m_hgtdID -> barrel_ec((hi)->cellID());
  Float_t xtemp = (((m_hgtdID->x_index( (hi)->cellID())) -1)*0.5 + 0.25)* zposneg;
  Float_t ytemp = ((m_hgtdID->y_index( (hi)->cellID())) -1)*0.5 + 0.25;
  int posNegEndcap = m_hgtdID -> barrel_ec((hi)->cellID());
  int sampling     = m_hgtdID -> sampling((hi)->cellID());
  int granularity  = m_hgtdID -> granularity((hi)->cellID());
  int xIndex;
  int yIndex;
  Identifier id;

  // int idx_test=0;
  // int idy_test=0;
  // Float_t x_test=0;
  // Float_t y_test=0;

  // m_Detector.pos_to_id(xtemp, ytemp, idx_test, idy_test);
  // m_Detector.id_to_pos(idx_test, idy_test, x_test, y_test);

  // if(m_Detector.is_connected(xtemp,ytemp) && (fabs(x_test-xtemp)>0.5 || fabs(y_test-ytemp)>0.5) ){
  //   std::cout<< " x0 : " << xtemp << " y0 : " << ytemp << std::endl;
  //   std::cout<< " idx0 : " << idx_test << " idy0 : " << idy_test << std::endl;
  //   std::cout<< " x1 : " << x_test << " y1 : " << y_test << std::endl;
  // }

  if(m_granu == true){
    // fill the new real indices xIndex and yIndex identifying each pad in a unique way (correct for deadzone)
    m_Detector.pos_to_id(xtemp, ytemp, xIndex, yIndex);
  }
  else{
    // if granu = false, we use the m_cellReductionFactor (by default pads of (1mm*1mm) )
    xIndex       = int( ( (xtemp - 0.25)*2 + 1 ) / m_cellReductionFactor );
    yIndex       = int( ( (ytemp - 0.25)*2 + 1 ) / m_cellReductionFactor );
  }

  // Now we check if the hit is in the detector or if he hit a dead zone
  // If he is in the detector we add it to the hitCollection :

  id = m_hgtdID -> channel_id(posNegEndcap, sampling, granularity, xIndex, yIndex);

  std::string side;

  if(m_overlap == true){
    if(sampling%2 == 0){
      side = "Top";
    }
    else{
      side = "Bottom";
    }
  }
  else if(m_overlap == false){
    side = "None";
  }

  if(m_Detector.is_connected(xtemp,ytemp,side) || m_granu == false ){

    double t = (hi)->time();
    double E = (hi)->energy();
    double timeBinf=0;

    if(m_doPulseAna ==true){
      // new method with pulse shape simulation from testbeam results (1ps timebin)
      timeBinf = t/0.001;
    }
    if(m_doPulseAna ==false){
      // old method to choose the bin time
      timeBinf = t/m_timeBinSize;
    }

    int timeBin = int(timeBinf);

    hits_t* hitCollection = 0;
    auto setForThisBin = m_timeBins.find( timeBin );

    if (setForThisBin == m_timeBins.end()) {
      // New time bin
      hitCollection = new hits_t;
      m_timeBins[ timeBin ] = hitCollection;
    }
    else {
      // Get the existing set of hits for this time bin.
      // Reminders:
      // setForThisBin = iterator (pointer) into the m_timeBins map
      // (*setForThisBin) = pair< G4int, m_hits_t* >
      // (*setForThisBin).second = m_hits_t*, the pointer to the set of hits

      hitCollection = (*setForThisBin).second;
    }

    LArHit* hit = new LArHit(id,E,t);

    // We calculate the number of hit per cell,
    // this will be use later for the summation of the pulses

    if (m_nbhit.find(id) == m_nbhit.end() )
    m_nbhit.insert(std::make_pair(id,1));
    else
    m_nbhit[id]=m_nbhit[id]+1;

    // If we haven't had a hit in this cell before, create one and add
    // it to the hit collection.

    // If we've had a hit in this cell before, then add the energy to
    // the existing hit.

    // Look for the key in the hitCollection (this is a binary search).
    auto bookmark = hitCollection->lower_bound(hit);

    // The lower_bound method of a map finds the first element
    // whose key is not less than the identifier.  If this element
    // == our hit, we've found a match.

    // Reminders:
    // bookmark = iterator (pointer) into the hitCollection set.
    // (*bookmark) = a member of the set, which is a LArG4Hit*.

    // Equals() is a function defined in LArG4Hit.h; it has the value of
    // "true" when a LArG4Hit* points to the same identifier.

    if (bookmark == hitCollection->end() || !(*bookmark)->Equals(hit)) {
      // We haven't had a hit in this readout cell before.  Add it
      // to our set.
      if (hitCollection->empty() || bookmark == hitCollection->begin()) {
        // Insert the hit before the first entry in the map.
        hitCollection->insert(hit);
      }
      else {
        // We'just done a binary search of hitCollection, so we should use
        // the results of that search to speed up the insertion of a new
        // hit into the map.  The "insert" method is faster if the new
        // entry is right _after_ the bookmark.  If we left bookmark
        // unchanged, the new entry would go right _before_ the
        // bookmark.  We therefore want to decrement the bookmark from
        // the lower_bound search.

        hitCollection->insert(--bookmark, hit);
      }
    }
    else {
      // Update the existing hit.
      (*bookmark)->Add(hit);

      // We don't need our previously-created hit anymore.
      delete hit;
    }
  }
}

void HGTDHitConversion::Pulse(LArHitContainer * hitContainer,const HGTD_ID * m_hgtdID) {
  std::map < Identifier, std::vector<LArHit*> >  hittemp;
  if(m_doPulseAna==true){
    std::srand(std::time(0));

    std::map<int,std::pair<double,double> > Pulsebin ;

    for(const auto i : m_timeBins) {
      const hits_t* hitSet = i.second;

      for(auto hit : *hitSet) {
        hit->finalize();
        if(m_nbhit[hit->cellID()]==1 && hit->energy()>0.02 ){
          // We transform the hits into pulses in 2 time, first we work with the cells with one hit in them
          Pulsebin.clear();
          double max_hit[4] = {0};

          Float_t x_hit=0;
          Float_t y_hit=0;
          m_Detector.id_to_pos(m_hgtdID->x_index( hit->cellID()), m_hgtdID->y_index( hit->cellID()), x_hit, y_hit);
          PulseSimulation(sqrt(x_hit*x_hit+y_hit*y_hit));


          PulseShape(Pulsebin, hit->time(), hit->energy(),max_hit);

          for(auto & pulse : Pulsebin) {
            pulse.second.first = pulse.second.first/pulse.second.second;

            // We look the the time when E=Emax/2 to get the time
            if((max_hit[1]>pulse.second.first) && (max_hit[0]*m_cfd<pulse.second.second) && (max_hit[3]>pulse.second.first || max_hit[3]==0 ) ){
              max_hit[2] = pulse.second.second;  // Energy when E=Emax*m_cfd
              max_hit[3] = pulse.second.first; // Time when E=Emax*m_cfd
            }
          }

          LArHit* hitf ;
          // We had the effect of the electronic noise
          if( m_Detector.CellID( (m_hgtdID->x_index(hit->cellID())),(m_hgtdID->y_index(hit->cellID())) ).granularity() == 1)
          hitf = new LArHit(hit->cellID(),max_hit[0],max_hit[3]+m_rand->Gaus(0,m_felec1));
          else
          hitf = new LArHit(hit->cellID(),0,0);
          hitf->finalize();

          hitContainer->push_back(hitf); // We add a hit in the container corresponding to 1 pad

        }
        else{
          // If there is more than one hit in the pad we had it to a vector to be computed later
          if (hittemp.find(hit->cellID()) == hittemp.end()) {
            std::vector <LArHit*> v = {hit};
            hittemp[hit->cellID()] = v ;
          }
          else {
            hittemp[hit->cellID()].push_back(hit);
          }
        }
      }
    }

    for(auto i : m_timeBins) delete i.second;

    m_timeBins.clear();
    // We do the same things as previously in the case when there is multiple hit in a pad,
    // only this time we sum the different pulses.

    for(auto hitid : hittemp) {
      if(m_nbhit[hitid.first]){
        double max_hit[4] = {0};
        Pulsebin.clear();
        const  std::vector <LArHit*> hitVect = hitid.second;
        for(auto hit : hitVect) {
          if(hit->cellID() == hitid.first){

            Float_t x_hit=0;
            Float_t y_hit=0;
            m_Detector.id_to_pos(m_hgtdID->x_index( hit->cellID()), m_hgtdID->y_index( hit->cellID()), x_hit, y_hit);
            PulseSimulation(sqrt(x_hit*x_hit+y_hit*y_hit));

            PulseShape(Pulsebin, hit->time(), hit->energy(),max_hit);
          }
        }
        for(auto &  pulse : Pulsebin) {
          pulse.second.first = pulse.second.first/pulse.second.second;
          if((max_hit[1]>pulse.second.first) && (max_hit[0]*m_cfd<pulse.second.second) && (max_hit[3]>pulse.second.first || max_hit[3]==0 ) ){
            max_hit[2] = pulse.second.second;
            max_hit[3] = pulse.second.first;
          }
        }

        LArHit* hitf ;
        if( m_Detector.CellID( (m_hgtdID->x_index(hitid.first)),(m_hgtdID->y_index(hitid.first)) ).granularity() == 1)
        hitf = new LArHit(hitid.first,max_hit[0],max_hit[3]+m_rand->Gaus(0,m_felec1));
        else
        hitf = new LArHit(hitid.first,0,0);

        hitf->finalize();
        hitContainer->push_back(hitf);
      }
    }
  }

  if(m_doPulseAna == false){
    // If m_doPulseAna == false we simply store the timebins hit in the container
    for(const auto i : m_timeBins) {

      const hits_t* hitSet = i.second;
      // For each hit in the set...
      for(auto hit : *hitSet){
        // Because of the design, we are sure this is going into the right hit container
        hit->finalize();
        hitContainer->push_back(hit);
      } // End of loop over hits in the set
    } // End of loop over time bins
    for(auto i : m_timeBins) delete i.second;
    m_timeBins.clear();
  }
}

void HGTDHitConversion::id_to_pos(int ix, int iy, Float_t& px, Float_t& py){
  if(m_granu==true)
  m_Detector.id_to_pos(ix, iy, px, py);
  if(m_granu==false){
    px = ((ix -1)*0.5 + 0.25)*m_cellReductionFactor;
    py = ((iy -1)*0.5 + 0.25)*m_cellReductionFactor;
  }
}

void HGTDHitConversion::pos_to_id(Float_t px, Float_t py, int& ix, int& iy){
  if(m_granu==true)
  m_Detector.pos_to_id(px, py, ix, iy);
  if(m_granu==false){
    ix = ((px/m_cellReductionFactor) - 0.25)*2 + 1;
    iy = ((py/m_cellReductionFactor) - 0.25)*2 + 1;
  }
}

void HGTDHitConversion::pos_Cell(Float_t px0, Float_t py0, Float_t& px, Float_t& py){
  if(m_granu==true){
    m_Detector.pos_Cell(px0, py0, px, py);
  }
  if(m_granu==false){
    int ix=0;
    int iy=0;
    ix = ((px0/m_cellReductionFactor) - 0.25)*2 + 1;
    iy = ((py0/m_cellReductionFactor) - 0.25)*2 + 1;
    px = ((ix -1)*0.5 + 0.25)*m_cellReductionFactor;
    py = ((iy -1)*0.5 + 0.25)*m_cellReductionFactor;
  }
}

bool HGTDHitConversion::is_connected(Float_t x, Float_t y, std::string side) const {

  if(m_granu==true){
    return(m_Detector.is_connected(x,y,side));
  }
  if(m_granu==false){
    return(true);
  }
  return(false);
}

Float_t HGTDHitConversion::granularity_xy(Float_t x, Float_t y) const {
  if(m_granu==true){
    return( m_Detector.CellXY(x,y).granularity() );
  }
  if(m_granu==false){
    return(m_cellReductionFactor*0.5);
  }
  return(-1);
}

Float_t HGTDHitConversion::granularity_id(int ix, int iy) const {
  if(m_granu==true){
    return( m_Detector.CellID(ix,iy).granularity() );
  }
  if(m_granu==false){
    return(m_cellReductionFactor*0.5);
  }
  return(-1);
}
