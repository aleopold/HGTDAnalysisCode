#ifndef HGTDCALCULATOR_H
#define HGTDCALCULATOR_H

/**
* @file HGTDReadoutSim/HGTDCalculator.h
* @author Corentin Allaire
* @date May 2016
* @brief Classes implementing the HGTD, including tools that help to determine
* if a hit is in a dead zone or not.
*/


#include <string>
#include <vector>
#include <utility>
#include <map>
#include <TH1F.h>
#include "Rtypes.h"
#include <iostream>

class HGTDCalculatorCell {

  /**
  * @brief Cells used by HGTDCalculator
  *
  * Those objects are use to simulate the dead zone whithin a cell. Each of those object is basically a mask that contain the information on which zone are in the corresponding cell.
  */

  // Glossary :
  // Cell : A part of the detector composed of a specific sensor area (which can be different between Cells). They can have
  // different geometry defined by the is_connected method. They are repesented
  // by the HGTDCalculatorCell object.
  // pad : Component of a sensor area, depending of the granularity they are
  // 1*1 mm² or 3*3 mm². Each sensor area is composed of 96 cell.

public:
  /** Constructor 0 */
  HGTDCalculatorCell();

  /** Constructor A */
  HGTDCalculatorCell(Float_t x, Float_t y, int x_cell, int y_cell, Float_t granularity, Float_t sizex, Float_t sizey, int sizeidx, int sizeidy, int cellid);

  /** Constructor B */
  HGTDCalculatorCell(Float_t x, Float_t y, int x_cell, int y_cell, Float_t granularity, Float_t sizex, Float_t sizey, int sizeidx, int sizeidy, int cellid, std::string type);

  /** Copy Constructor */
  HGTDCalculatorCell( const HGTDCalculatorCell& Cell );

  /** Operator = */
  void operator=(const HGTDCalculatorCell& Cell);

  /** Operator < */
  bool operator<(const HGTDCalculatorCell& Cell) const;

  /** Operator == */
  bool operator==(const HGTDCalculatorCell& Cell) const;

  /** Position along the x-axis of the bottom left corner of the Cell [mm]. */
  Float_t x() const {return m_x;};

  /** Position along the y-axis of the bottom left corner of the Cell [mm]. */
  Float_t y() const {return m_y;};

  /** Id x of the sensor pad in the bottom left of the Cell. */
  int x_cell() const {return m_x_cell;};

  /** Id y of the sensor pad in the bottom left of the Cell. */
  int y_cell() const {return m_y_cell;};

  /** Granularity of the Cell [mm]. */
  Float_t granularity() const {return m_granularity;};

  /** Size of the Cell in the x direction [mm]. */
  Float_t sizex() const {return m_sizex;};

  /** Size of the Cell in the y direction [mm]. */
  Float_t sizey() const {return m_sizey;};

  /** Number of pads in the Cell in the x direction. */
  int sizeidx() const {return m_sizeidx;};

  /** Number of pads in the Cell in the y direction. */
  int sizeidy() const {return m_sizeidy;};

  /** Return the Cell Id (from 0 to 99) */
  int cellid() const {return m_cellid;};

  /** Type of Cell: this will be used to define the geometry of the dead zones. */
  std::string type() const {return m_type;};

  /** Tells the user if a hit at the provided coordinates is in a conected pad or a dead zone. */
  bool  is_connected(Float_t x, Float_t y) const;//return true if (x,y) is not in a dead zone

  /** Give the position of the center of the pad in the sensor from its x and y indices */
  std::pair<Float_t,Float_t> cell_pos(int x, int y) const;

  /** Give the id of the pad at coordinate (x,y) in the sensor */
  std::pair<int,int> cell_ID(Float_t x, Float_t y) const;

private:

  Float_t m_x;
  Float_t m_y;
  int m_x_cell;
  int m_y_cell;
  Float_t m_granularity;
  Float_t m_sizex;
  Float_t m_sizey;
  int m_sizeidx;
  int m_sizeidy;
  int m_cellid;
  std::string m_type;
};


class HGTDCalculator {

  /**
  * @brief Tool that tells the user if a hit is in a dead zone or not.
  *
  * This tool looks in which HGTDCalculatorCell the hit is, then it use its is_connected() method to tell if the hit is in an instrumented region or not.
  */

public:

  /** Constructor A */
  HGTDCalculator();

  /** Constructor B */
  HGTDCalculator(std::string build_name);

  /** Copy Constructor */
  HGTDCalculator(const HGTDCalculator& Calculator);

  /** Operator = */
  void operator=(const HGTDCalculator& Calculator);

  /** Build a detector using the standand geometry */
  void build();

  /** Build a detector using a geometry specify by the user */
  void build(std::string build_name);

  /** Get the Cell that contains the point at (x,y) */ // TODO: nomenclature should be clarified later /CO
  HGTDCalculatorCell CellXY(Float_t x, Float_t y) const;

  /** Get the Cell that contains the cell with indices (x,y) */ // TODO: same as above /CO
  HGTDCalculatorCell CellID(int x, int y) const;

  /** Tells the user if the point at (x,y) is connected or not */
  bool is_connected (Float_t x, Float_t y, std::string side) const ;

  /** Tells the user if the point at (x,y) is connected or not */
  bool overlap_hole (Float_t x, Float_t y, std::string side) const ;

  /** Get the x-y position given the the x and y pad indices */
  void id_to_pos(int ix, int iy, Float_t& px, Float_t& py);

  /** Get the x and y indices for a pad given its x-y coordinates */
  void pos_to_id(Float_t px, Float_t py, int& ix, int& iy);

  /** Get the position of the center of the pad that a hit with the provided coordinates will traverse */
  void pos_Cell(Float_t px0, Float_t py0, Float_t& px, Float_t& py);

  template < class T >
  /** Transform a position into the corresponding position in a different quadrant */
    int symmetry(T& x, T& y, int quad) const{
    auto temp = x;

    if(quad==0){
      if(x<0){
	if(y<0){
	  if(m_build_name=="standard"){
	    x = -x;
	    y = -y;
	  }
	  if(m_build_name=="diagonale" || m_build_name=="Helix_0" || m_build_name=="Helix_1" || m_build_name=="Helix_2" || m_build_name=="Helix_0_l2" || m_build_name=="Helix_1_l2" || m_build_name=="Helix_2_l2"){
	    x = -x;
	    y = -y;
	  }
	  return 3;
	}
	if(m_build_name=="standard"){
	  x = -x;
	  y = y;
	}
	if(m_build_name=="diagonale" || m_build_name=="Helix_0" || m_build_name=="Helix_1" || m_build_name=="Helix_2" || m_build_name=="Helix_0_l2" || m_build_name=="Helix_1_l2" || m_build_name=="Helix_2_l2"){
	  x = y;
	  y = -temp;
	}
	return 1;
      }
      if(y<0){
	if(m_build_name=="standard"){
	  x = x;
	  y = -y;
	}
	if(m_build_name=="diagonale" || m_build_name=="Helix_0" || m_build_name=="Helix_1" || m_build_name=="Helix_2" || m_build_name=="Helix_0_l2" || m_build_name=="Helix_1_l2" || m_build_name=="Helix_2_l2"){
	  x = -y;
	  y = temp;
	}
	return 2;
      }
      return 0;
    }

    if(quad==1){
      if(m_build_name=="standard"){
	x = -x;
	y = y;
      }
      if(m_build_name=="diagonale" || m_build_name=="Helix_0" || m_build_name=="Helix_1" || m_build_name=="Helix_2" || m_build_name=="Helix_0_l2" || m_build_name=="Helix_1_l2" || m_build_name=="Helix_2_l2"){
	x = -y;
	y = temp;
      }
      return 0;
    }

    if(quad==2){
      if(m_build_name=="standard"){
	x = x;
	y = -y;
      }
      if(m_build_name=="diagonale" || m_build_name=="Helix_0" || m_build_name=="Helix_1" || m_build_name=="Helix_2" || m_build_name=="Helix_0_l2" || m_build_name=="Helix_1_l2" || m_build_name=="Helix_2_l2"){
	x = y;
	y = -temp;
      }
      return 0;
    }

    if(quad==3){
      if(m_build_name=="standard"){
	x = -x;
	y = -y;
      }
      if(m_build_name=="diagonale" || m_build_name=="Helix_0" || m_build_name=="Helix_1" || m_build_name=="Helix_2" || m_build_name=="Helix_0_l2" || m_build_name=="Helix_1_l2" || m_build_name=="Helix_2_l2"){
	x = -x;
	y = -y;
      }
      return 0;
    }

    std::cout << "WRONG QUADRANT NUMBER USE" << std::endl;
    return -1;
  }

  /** Get the outer radius of the detector */
  unsigned int rmax() const {return(m_rmax);};

  /** Tells the user if the HGTDCalculator is ready to be used or not */
  bool ready() const {return(m_ready);};

  /** Get a vector of the cell objects that constitute the detector  */
  std::vector <HGTDCalculatorCell> det() const {return(m_det);}; // TODO: could this function get a more descriptive name? /CO

  /** Get the name of the geometry */
  std::string build_name() const {return(m_build_name);};


private:
  /// Max radius of the detector
  unsigned int m_rmax=600;

  /// Is the detector ready to be used
  bool m_ready = false ;

  /// Detector = vector of Cells
  std::vector <HGTDCalculatorCell> m_det;

  /// Name of the geometry
  std::string m_build_name;

};

#endif // HGTDCALCULATOR_H
