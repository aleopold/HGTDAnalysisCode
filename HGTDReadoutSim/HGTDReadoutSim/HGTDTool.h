#ifndef HGTD_TOOL_H
#define HGTD_TOOL_H

#include "CaloIdentifier/HGTD_ID.h"
#include "HGTDReadoutSim/HGTDCalculator.h"
#include "HGTDReadoutSim/HGTDHitConversion.h"
#include "HGTDReadoutSim/HGTDTimingResolution.h"
#include "HGTDReadoutSim/HGTDTrackExtrapolator.h"



/**
* @file HGTDReadoutSim/HGTDTool.h
* @author Corentin Allaire
* @date January 2018
* @brief Structure containing all the usful tool related to the HGTD
*/

struct HGTD_TOOL{

  const HGTD_ID * hgtdID; // Use to extract ID from hit
  HGTDCalculator * Det_l[4]; // Geometry of the HGTD
  HGTDHitConversion * Conv_l[4]; // Transformation of the hits according to the geometry
  HGTDTimingResolution * Timing; // Timing resolution as function of R
  HGTDTrackExtrapolator * hgtd_extrapolator; // Extrapolation of the ITk track up to the HGTD

};

#endif // HGTD_TOOL_H
