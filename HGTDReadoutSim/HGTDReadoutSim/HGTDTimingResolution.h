#ifndef HGTDTIMINGRESOLUTION_H
#define HGTDTIMINGRESOLUTION_H

#include <string>
#include <vector>

enum HGTDTimingEnum {HGTDInitialResolution, HGTDFinalResolution, HGTDIntermediateResolution};

class HGTDTimingResolution 
{
 public:

  HGTDTimingResolution();
  HGTDTimingResolution(unsigned int, double Rmin=130., double Rmax=650., double zposition=3500.);

  // Initialize and check
  void Init(double);
  bool RInRange(double);
  bool EtaInRange(double);
  
  // methods
  double TimingResolutionPerHit4R(double);
  double TimingResolutionPerTrack4Eta(double);
  double TimingResolutionPerTrack4R(double);
  double AverageNhitsPerTrack4R(double);

  bool changeResolution2(unsigned int);
  double Resolution(double);
  double ElectronicsResolution();

  double TranslateR2Eta(double);
  double TranslateEta2R(double);

  void Print();

 private:

  std::string m_version;

  unsigned int m_sigmaType; 

  double m_Rmin;
  double m_Rmax;

  double m_Zposition;

  double m_electronicsResolution;
  std::vector<double> m_finalResolution;
  std::vector<double> m_intermediateResolution;

};
#endif
