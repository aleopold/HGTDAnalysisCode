#ifndef HGTD_EXTRAPOLATOR_H
#define HGTD_EXTRAPOLATOR_H


#include "GaudiKernel/ToolHandle.h"
#include "xAODTracking/TrackParticle.h"

// Track extrapol
#include "TrkExInterfaces/IExtrapolator.h"
#include "TrkSurfaces/DiscSurface.h"

#include "TVector3.h"

/**
* @file HGTDReadoutSim/HGTDTrackExtrapolator.h
* @author Corentin Allaire
* @date January 2018
* @brief Tool that extrapolate the ITk track up to the 4 layers of the HGTD.
*/

class HGTDTrackExtrapolator {

  /**
  * @brief Tool that extrapolate the ITk track up to the 4 layers of the HGTD.
  *
  * The first two layers correspond to the two side of the first cooling plate
  * while the last two correspond to the two side of the second cooling plate.
  */

public:
  /** Constructor 0 */
  HGTDTrackExtrapolator();

  /** Constructor A */
  HGTDTrackExtrapolator(ToolHandle<Trk::IExtrapolator> * extrapolator);

  /**  Set the extrapolator used in the extrapolation */
  void SetExtrapolator(ToolHandle<Trk::IExtrapolator> * extrapolator);

  /**
  * Extrapolate the track : "trackParticle" up the the layer : "surface" of the HGTD.
  * The vector "v" and "vp" correspond to the position and the impultion of the particule after extrapolation.
  * Return true if the extrapolation is succesfull and false if it fail.
  */
  bool Track_Extrapolation(const xAOD::TrackParticle* trackParticle, TVector3 & v, TVector3 & vp, int surface);

 private:

   /** Extrapolator used in the extrapolation */
  ToolHandle<Trk::IExtrapolator> * m_extrapolator;

  /** Surface corresponding to the top of the first layer of the HGTD with z>0 */
  Trk::DiscSurface* m_discSurface_atCaloEntrance_positiveZ1_top;
  /** Surface corresponding to the top of the first layer of the HGTD with z<0 */
  Trk::DiscSurface* m_discSurface_atCaloEntrance_negativeZ1_top;

  /** Surface corresponding to the bottom of the first layer of the HGTD with z>0 */
  Trk::DiscSurface* m_discSurface_atCaloEntrance_positiveZ2_top;
  /** Surface corresponding to the bottom of the first layer of the HGTD with z<0 */
  Trk::DiscSurface* m_discSurface_atCaloEntrance_negativeZ2_top;

  /** Surface corresponding to the top of the second layer of the HGTD with z>0 */
  Trk::DiscSurface* m_discSurface_atCaloEntrance_positiveZ1_bottom;
  /** Surface corresponding to the top of the second layer of the HGTD with z<0 */
  Trk::DiscSurface* m_discSurface_atCaloEntrance_negativeZ1_bottom;

  /** Surface corresponding to the bottom of the second layer of the HGTD with z>0 */
  Trk::DiscSurface* m_discSurface_atCaloEntrance_positiveZ2_bottom;
  /** Surface corresponding to the bottom of the second layer of the HGTD with z<0 */
  Trk::DiscSurface* m_discSurface_atCaloEntrance_negativeZ2_bottom;

};

#endif // HGTD_EXTRAPOLATOR_H
