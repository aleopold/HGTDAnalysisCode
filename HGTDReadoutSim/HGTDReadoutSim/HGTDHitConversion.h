#ifndef HGTD_HIT_CONVERSION_H
#define HGTD_HIT_CONVERSION_H

#include "HGTDReadoutSim/HGTDCalculator.h"
#include "HGTDReadoutSim/HGTDTimingResolution.h"
#include "LArSimEvent/LArHitContainer.h"

#include "TRandom3.h"
#include "TString.h"
#include "TFile.h"
#include "TH1.h"

class HGTD_ID;

/**
* @file HGTDReadoutSim/HGTDHitConversion.h
* @author Corentin Allaire
* @date December 2016
* @brief Tool that transform the input hits into new hits after simulating the requested granularity,
* and pulses for each hit and the expected effects of resolution due to noise etc.
*/

class HGTDHitConversion {

  /**
  * @brief Tool that transform the input hits into new hits after simulating the requested granularity,
* and pulses for each hit and the expected effects of resolution due to noise etc.
  *
  * This class can also be used to change the way the hit conversion is done and to transform indices into positions.
  */

public:
  /** Constructor 0 */
  HGTDHitConversion();

  /** Constructor A */
  HGTDHitConversion(bool j,bool k, bool l);

  /** Constructor B */
  HGTDHitConversion(bool j,bool k, bool l, std::string geo, float timeBinSize=0.2);

/** Set the timing resolution of the HGTD */
  void SetResolution(HGTDTimingResolution * Timing);

  /** Set the cellReductionFactor, i.e. the factor with which pads are merged to go from simulated granularity (0.5 mm x 0.5 mm) to the granularity requested by the user - NB! This is only used if a geometry is not requested by the user!  */
  void SetcellReductionFactor(int i);

  /** Set the constant fraction discriminator fraction */
  void Setcfd(double f); // TODO: the function name suggests the CFD is set, not the *threshold* of the CFD /CO

  /** Simulate a new pulse that can be acess using the PulseShape method */
  void PulseSimulation(double R);

  /** Return the Pulse as a vector of double (400 points) */
  void PulseShape(std::map<int,std::pair<double,double> > &Pulsebin, double t, double E, double *max);

  /** Check whether the granularity will be taken from a requested geometry or not (in the latter case, the merging factor cellReductionFactor above is used) */
  bool Getgranu() const {return(m_granu);}; // TODO: this function needs a more descriptive name (it doesn't return a granularity) /CO

  /** Return if the a pulse is created for each hit or not */
  bool Getpulse() const {return(m_doPulseAna);};

  /** Return the cellReductionFactor */
  int GetcellReductionFactor() const {return(m_cellReductionFactor);};

  /** Associate a hit to the appropriate time bin */
  void TimeBin(LArHit * hi, const HGTD_ID * m_hgtdID);

  /** Takes all the hits in the time bins, creates for each hit a pulse,
  computes one hit per cell using the sum of those pulses and stores it in the hitContainer */
  void Pulse(LArHitContainer * hitContainer, const HGTD_ID * m_hgtdID);

  /** Provided the x and y indices of a cell, this function returns the x-y position  */
  void id_to_pos(int ix, int iy, Float_t& px, Float_t& py);

  /** Provided a position, this function returns the x and y indices of the associated cell */
  void pos_to_id(Float_t px, Float_t py, int& ix, int& iy);

  /** Get the position of the center of the cell in function of the position */ // TODO: same comment as for the corresponding function in HGTDCalculator.h - I don't understand the explanation! /CO
  void pos_Cell(Float_t px0, Float_t py0, Float_t& px, Float_t& py);

  /** Tell the user if the point of coordinate (x,y) is connected or not. */
  bool is_connected (Float_t x, Float_t y, std::string side) const;

  /** Get the granularity of the cell at the position (x,y) */ // TODO: "the granularity of a cell" - is this really the *size* of the cell? /CO
  Float_t granularity_xy(Float_t x, Float_t y) const;

  /** Get the granularity of the cell with the id (ix,iy) */
  Float_t granularity_id(int ix, int iy) const;

  double exp16(double x) {
    x = 1.0 + x / 16.0;
    x *= x; x *= x; x *= x; x *= x;
    return x;
  }

private:

  bool m_granu=true; //  true == the real granularity is implemented
                     //  The dead zones need to be activated or it may cause problem
                     //  (hit with coordinate (0,0) will be created in the dead zones)

  bool m_doPulseAna=true; //  true == convert each hit into a pulse (400 hits) to compute E and t

  bool m_overlap=true; // true == add the inter-module dead zone, to match the current hgtd baseline.
                       // in that case layer 1 and 2 will correspond to both side of the first cooling plate
                       // and layer 3 and 4 to bothside of the second one. 
                       // (this is done by seting "side" to Top or Bottom in the is_connected methode) 

  /** time bin size for old time determination method.
      ONLY RELEVANT IF m_doPulseAna = false!
  ***/
  const float m_timeBinSize;

  int m_cellReductionFactor=1; // (Size of the cells)*2, use to take into account the granularity when m_granu == false

  double m_felec1=0.025; //factor use to simulate the electronic noise (1mm*1mm)

  double m_cfd=0.5; // Fraction of Emax at which the time is define (by default : E=Emax/2 )

  std::map<Identifier,unsigned int > m_nbhit; // Map of the number of hit in each cells,
  // use for the summation of the pulses

  HGTDCalculator m_Detector; // Class use for the simulation of the dead zone
  // and the conversion fron cell id to position

  const HGTD_ID * m_hgtdID; // Class use to get the information on the hits

  TRandom3 * m_rand = new TRandom3(0); // TOOD: needs a delete call! /CO

  double m_IPulse[400]={0}; // Signal Pulse

  HGTDTimingResolution * m_Timing; // Provide the timing as function of R

  // The set defined below is used to tell us if we've already had a
  // hit in a cell.  We store these hits in a set, so we can quickly
  // search it.  Note the use of a custom definition of a "less"
  // function for the set, so we're not just comparing hit pointers.
  class LessHit {
  public:
    bool operator() ( LArHit* const& p, LArHit* const& q ) const
    {
      return p->Less(q);
    }
  };

  typedef std::set< LArHit*, LessHit >  hits_t;
  // The hits are grouped into time bins, with the width of a bin
  // determined by a user parameter.  This map is used to associate a
  // time bin with its corresponding set of hits.
  typedef std::map < int, hits_t* >   timeBins_t;
  timeBins_t m_timeBins;

};

#endif // HGTD_HIT_CALCULATOR_H
