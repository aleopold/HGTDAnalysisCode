#from AthenaCommon.AppMgr import ToolSvc
from AthenaCommon.AppMgr import ServiceMgr
import AthenaPoolCnvSvc.ReadAthenaPool

from PartPropSvc.PartPropSvcConf import PartPropSvc

include( "ParticleBuilderOptions/McAOD_PoolCnv_jobOptions.py")
include( "EventAthenaPool/EventAthenaPool_joboptions.py" )

import os
from glob import glob
from AthenaCommon.AthenaCommonFlags  import athenaCommonFlags
# Use "athenaCommonFlags.FilesInput += ..." to read multiple files
#athenaCommonFlags.FilesInput = glob( "/tmp/"+os.environ['USER']+"HITS*root*" )

#=======================================================#
#================== Muons 1TeV MU0  Si =================#

athenaCommonFlags.FilesInput = glob("/eos/atlas/atlascerngroupdisk/det-hgtd/samples/aod/20.20.7.2/Muons/Si/MU0/eta32_43/*")

athenaCommonFlags.FilesInput += glob("/eos/atlas/atlascerngroupdisk/det-hgtd/samples/aod/20.20.7.2/Muons/Si/MU0/eta23_32/mc15_14TeV.415021.ParticleGun_single_muon_Pt45_etaFlatnp23_32.recon.AOD.e5580_s3072_s3059_r8956/*")

#athenaCommonFlags.FilesInput += glob("AOD.10654071.*")


ServiceMgr.EventSelector.InputCollections = athenaCommonFlags.FilesInput() # This is stupid and redundant, but necessary

from AthenaCommon.AlgSequence import AlgSequence
topSequence = AlgSequence()

from HGTDHitAnalysis.HGTDHitAnalysisConf import HGTDHitAnalysis
topSequence += HGTDHitAnalysis()
HGTDHitAnalysis = HGTDHitAnalysis()
HGTDHitAnalysis.HGTDHitsContainerName = 'HGTDDigitContainer_MC' # 'HGTDDigitContainer_MC' for RDO input (default), 'LArHitHGTD' for HITS input
HGTDHitAnalysis.NtupleFileName = 'HGTDHitAnalysis'

from GaudiSvc.GaudiSvcConf import THistSvc
ServiceMgr += THistSvc()

#ServiceMgr.THistSvc.Output += [ "HGTDHitAnalysis DATAFILE='HGTDHitAnalysis_Initial.root' OPT='RECREATE'" ]
#ServiceMgr.THistSvc.Output += [ "HGTDHitAnalysis DATAFILE='HGTDHitAnalysis_Intermediate.root' OPT='RECREATE'" ]
#ServiceMgr.THistSvc.Output += [ "HGTDHitAnalysis DATAFILE='HGTDHitAnalysis_Final.root' OPT='RECREATE'" ]
ServiceMgr.THistSvc.Output += [ "HGTDHitAnalysis DATAFILE='TEST.root' OPT='RECREATE'" ]

ServiceMgr.MessageSvc.OutputLevel = INFO
ServiceMgr.MessageSvc.defaultLimit = 9999999

theApp.EvtMax = 50
#theApp.EvtMax = 200000
#theApp.EvtMax = -1

ServiceMgr.AuditorSvc.Auditors  += [ "ChronoAuditor"]

AthenaPoolCnvSvc = Service("AthenaPoolCnvSvc")
AthenaPoolCnvSvc.UseDetailChronoStat = TRUE

###############################################################
##### Import the Tracking Geometry for the extrapolation ######
###############################################################

from AthenaCommon.GlobalFlags import globalflags
#globalflags.DetGeo = 'atlas'

from RecExConfig.InputFilePeeker import inputFileSummary
globalflags.DataSource = 'data' if inputFileSummary['evt_type'][0] == "IS_DATA" else 'geant4'
globalflags.DetDescrVersion = inputFileSummary['geometry']

xmlTags = [["ATLAS-P2-ITK-10-01-01","InclBrl_4","GMX"],
           ["ATLAS-P2-ITK-10-02-01","InclBrl_4","GMX"],]

for geoTag, layoutDescr, gmx in xmlTags:
   if (globalflags.DetDescrVersion().startswith(geoTag)):
      print "preIncludes for ",layoutDescr, " layout"
      from InDetRecExample.InDetJobProperties import InDetFlags
      include('InDetSLHC_Example/preInclude.SLHC.SiliconOnly.Reco.py')
      include('InDetSLHC_Example/preInclude.SLHC_Setup_'+layoutDescr+'.py')
      if gmx=="GMX":
         print "preIncludes for GMX strip layout"
         include('InDetSLHC_Example/preInclude.SLHC_Setup_Strip_GMX.py')
         if geoTag=="ATLAS-P2-ITK-10-00-00" or geoTag=="ATLAS-P2-ITK-09-00-00" :
            include('InDetSLHC_Example/SLHC_Setup_Reco_TrackingGeometry.py')
         else:
            include('InDetSLHC_Example/SLHC_Setup_Reco_TrackingGeometry_GMX.py')
      else :
         include('InDetSLHC_Example/SLHC_Setup_Reco_TrackingGeometry.py')
      break

###############################################################

# To set up a geometry
#include("RecExCond/RecExCommon_DetFlags.py")
from AthenaCommon.DetFlags  import DetFlags
DetFlags.dcs.all_setOn()
DetFlags.detdescr.all_setOn()
DetFlags.makeRIO.all_setOn()
# these have to be set off by hand!
DetFlags.TRT_setOff()
DetFlags.pixel_setOn() # change from off to on
DetFlags.detdescr.BField_setOn() # just added
DetFlags.detdescr.SCT_setOn() # just added
# these have to be set On by hand!
DetFlags.dcs.HGTD_setOn()
DetFlags.detdescr.HGTD_setOn()
DetFlags.makeRIO.HGTD_setOn()


# Set up geometry and BField (also added for extrapol)
include("RecExCond/AllDet_detDescr.py")

from InDetSLHC_Example.SLHC_JobProperties import SLHC_Flags
SLHC_Flags.SLHC_Version = ''


from AthenaCommon.GlobalFlags import jobproperties
DetDescrVersion = jobproperties.Global.DetDescrVersion()

for geoTag, layoutDescr,gmx in xmlTags:
   if (globalflags.DetDescrVersion().startswith(geoTag)):
      print "postInclude for ",layoutDescr, " layout"
      include('InDetSLHC_Example/postInclude.SLHC_Setup_'+layoutDescr+'.py')
      break


from RecExConfig.AutoConfiguration import *
ConfigureFieldAndGeo() # Configure the settings for the geometry
include("RecExCond/AllDet_detDescr.py") # Actually load the geometry
#include( "TrkDetDescrSvc/AtlasTrackingGeometrySvc.py" ) # Tracking geometry, handy for ID work
#DetFlags.pixel_setOff()

#NEW STUFF for GMX:
# Tools configuration of the GMX based Strip description

from SCT_GeoModelXml.SCT_GeoModelXmlConf import SCT_GMX_DetectorTool
SCT_GMX_DetectorTool.GmxFilename = "ITkStrip.gmx"
SCT_GMX_DetectorTool.OutputLevel = VERBOSE

from AthenaCommon.CfgGetter import getPublicTool
getPublicTool("SLHC_SctSensorSD").GmxSensor=True
