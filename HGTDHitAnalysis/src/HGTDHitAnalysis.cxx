// Section of includes for LAr calo tests
#include "LArSimEvent/LArHitContainer.h"
#include "CaloDetDescr/CaloDetDescrElement.h"
#include "CaloDetDescr/CaloDetDescrManager.h"
#include "GeoAdaptors/GeoLArHit.h"

// Section of includes for tile calo tests
#include "TileDetDescr/TileDetDescrManager.h"
#include "CaloIdentifier/TileID.h"
#include "TileSimEvent/TileHit.h"
#include "TileSimEvent/TileHitVector.h"

#include "CaloIdentifier/HGTD_ID.h"

#include "GaudiKernel/ITHistSvc.h"

#include "EventInfo/EventInfo.h"
#include "EventInfo/EventID.h"

// Track extrapol
#include "TrkExInterfaces/IExtrapolator.h"
#include "CaloTrackingGeometry/ICaloSurfaceBuilder.h"
#include "TrkSurfaces/DiscSurface.h"

#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/TrackParticle.h"

// implementation of the tracks

#include "GeneratorObjects/McEventCollection.h"
#include "HepMC/GenEvent.h"


// implementation of the HGTD Tools

#include "HGTDHitAnalysis/HGTDHitAnalysis.h"
#include "HGTDReadoutSim/HGTDCalculator.h"
#include "HGTDReadoutSim/HGTDHitConversion.h"
#include "HGTDHitAnalysis/HGTDTrack.h"


#include "TFile.h"
#include "TH1.h"
#include "TTree.h"
#include "TString.h"
#include "TMath.h"
#include "TGraphErrors.h"
#include "TF1.h"
#include "TRandom.h"

// for the collection
#include "CxxUtils/make_unique.h"
#include <algorithm>
#include <math.h>
#include <functional>
#include <iostream>


HGTDHitAnalysis::HGTDHitAnalysis(const std::string& name, ISvcLocator* pSvcLocator)
  : AthAlgorithm(name, pSvcLocator)
  , m_tree(0)
  , m_ntupleFileName("/ntuples/file1")
  , m_ntupleDirName("HGTDHitAnalysis")
  , m_ntupleTreeName("HGTDHitAna")
  , m_hgtdHitsContName("HGTDDigitContainer_MC") // default is for running on RDO input, set to "LArHitHGTD" when running on HITS input
  , m_thistSvc(0)
  , m_tileID(0)
  , m_tileMgr(0)
  , m_extrapolator("Trk::Extrapolator/AtlasExtrapolator")
  , m_nevent(0)
{
  declareProperty("NtupleFileName", m_ntupleFileName);
  declareProperty("NtupleDirectoryName", m_ntupleDirName);
  declareProperty("NtupleTreeName", m_ntupleTreeName);
  declareProperty("HGTDHitsContainerName", m_hgtdHitsContName);
  declareProperty("EventNumber", m_nevent);
}

HGTDHitAnalysis::~HGTDHitAnalysis()
{;}

StatusCode HGTDHitAnalysis::initialize() {
  ATH_MSG_DEBUG( "Initializing HGTDHitAnalysis" );

  StatusCode sc = detStore()->retrieve(m_tileMgr);
  if (sc.isFailure()) {
    ATH_MSG_ERROR( "Unable to retrieve TileDetDescrManager from DetectorStore" );
    m_tileMgr=0;
  }

  sc = detStore()->retrieve(m_tileID);
  if (sc.isFailure()) {
    ATH_MSG_ERROR( "Unable to retrieve TileID helper from DetectorStore" );
    m_tileID=0;
  }

  // retrieve the HGTD_ID
  sc = detStore()->retrieve(m_hgtdID);
  if (sc.isFailure()) {
    ATH_MSG_ERROR( "Unable to retrieve HGTD_ID helper from DetectorStore" );
    m_hgtdID=0;
  }

  // Grab the Ntuple and histogramming service for the tree
  sc = service("THistSvc",m_thistSvc);
  if (sc.isFailure()) {
    ATH_MSG_ERROR( "Unable to retrieve pointer to THistSvc" );
    return sc;
  }

  ///////////////////////////////////////////////////////////////////////////

  m_tree = new TTree( TString(m_ntupleTreeName), "HGTDHitAna" );
  std::string fullNtupleName =  "/"+m_ntupleFileName+"/"+m_ntupleDirName+"/"+m_ntupleTreeName;
  sc = m_thistSvc->regTree(fullNtupleName, m_tree);
  if (sc.isFailure()) {
    ATH_MSG_ERROR("Unable to register TTree: " << fullNtupleName);
    return sc;
  }

  /** now add branches and leaves to the tree */
  if (m_tree){
    //main tree : one entry per event
    m_tree->Branch("granularity", &m_granularity);
    m_tree->Branch("nCells0", &m_nCells0);
    m_tree->Branch("nCells1", &m_nCells1);
    m_tree->Branch("nCells2", &m_nCells2);
    m_tree->Branch("nCells3", &m_nCells3);
    m_tree->Branch("emax0", &m_emax0);
    m_tree->Branch("emax1", &m_emax1);
    m_tree->Branch("emax2", &m_emax2);
    m_tree->Branch("emax3", &m_emax3);
    m_tree->Branch("Rmax0", &m_Rmax0);
    m_tree->Branch("Rmax1", &m_Rmax1);
    m_tree->Branch("Rmax2", &m_Rmax2);
    m_tree->Branch("Rmax3", &m_Rmax3);
    m_tree->Branch("tmax0", &m_tmax0);
    m_tree->Branch("tmax1", &m_tmax1);
    m_tree->Branch("tmax2", &m_tmax2);
    m_tree->Branch("tmax3", &m_tmax3);
    m_tree->Branch("SampHit",&m_SampHit);
    m_tree->Branch("nevent",&m_nevent);
  }

  ///////////////////////////////////////////////////////////////////////////

  m_treeHit = new TTree( TString("Hits"), "HGTDHitAna" );
  fullNtupleName = "/"+m_ntupleFileName+"/"+m_ntupleDirName+"/Hits";
  sc = m_thistSvc->regTree(fullNtupleName, m_treeHit);
  if (sc.isFailure()) {
    ATH_MSG_ERROR("Unable to register TTree: " << fullNtupleName);
    return sc;
  }

  if (m_treeHit){
    m_treeHit->Branch("coordx",&m_coordx);
    m_treeHit->Branch("coordy",&m_coordy);
    m_treeHit->Branch("coordr",&m_coordr);
    m_treeHit->Branch("energy",&m_energy);
    m_treeHit->Branch("time",&m_time);
    m_treeHit->Branch("treco",&m_treco);
    m_treeHit->Branch("nsamp",&m_nsamp);
    m_treeHit->Branch("nevent",&m_nevent);
    m_treeHit->Branch("zposneg", &m_zposneg);
    m_treeHit->Branch("granularity",&m_granularity);
  }

  ///////////////////////////////////////////////////////////////////////////

  m_treeTrack = new TTree( TString("Track"), "HGTDHitAna" );
  fullNtupleName = "/"+m_ntupleFileName+"/"+m_ntupleDirName+"/Track";
  sc = m_thistSvc->regTree(fullNtupleName, m_treeTrack);
  if (sc.isFailure()) {
    ATH_MSG_ERROR("Unable to register TTree: " << fullNtupleName);
    return sc;
  }

  if (m_treeTrack){
    m_treeTrack->Branch("track_eta",&m_track_eta);
    m_treeTrack->Branch("track_R",&m_track_R);
    m_treeTrack->Branch("track_Pt",&m_track_Pt);
    m_treeTrack->Branch("track_hit",&m_track_hit);
    m_treeTrack->Branch("track_time",&m_track_time);
    m_treeTrack->Branch("track_time_res",&m_track_time_res);
    m_treeTrack->Branch("nevent",&m_nevent);
  }

  ///////////////////////////////////////////////////////////////////////////

  m_nevent=0; // to initialise to the event number of the last analysis


  ////////////////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////// Possible Options : ////////////////////////////////////////////

  m_pulse = true; // true == convert each hit into a pulse (400 hits) to
                     // compute E and t.

  m_granu = true; // is the real granularity implemented

  m_overlap = true; // true == add the inter-module dead zone, to match the current hgtd baseline.
                    // in that case layer 1 and 2 will correspond to both side of the first cooling plate
                    // and layer 3 and 4 to bothside of the second one. 
                    // (this is done by seting "side" to Top or Bottom in the is_connected methode) 


  m_Geo = "Helix_2";

  ////////////////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////////////////////

  m_Timing = HGTDTimingResolution(); // Set the Tool that give the resolution as function of R and luminosity 

  m_Timing.changeResolution2(HGTDTimingEnum::HGTDInitialResolution);
  //m_Timing.changeResolution2(HGTDTimingEnum::HGTDIntermediateResolution);
  //m_Timing.changeResolution2(HGTDTimingEnum::HGTDFinalResolution);

  m_Timing.Init(3500);

  m_Det = new HGTDCalculator(m_Geo); // Class that can be use to get specific information on a Cell
  m_Conv = new HGTDHitConversion(m_granu, m_pulse, m_overlap,m_Geo); // Class use to take into account the effect of the deadzones, the proper granularity and the signal pulse

  m_Det_l2 = new HGTDCalculator(m_Geo+"_l2"); // Correspond to the second layer, mirror symmetry of the first one
  m_Conv_l2 = new HGTDHitConversion(m_granu, m_pulse, m_overlap, m_Geo+"_l2"); // Correspond to the second layer, mirror symmetry of the first one

  // Set the resolution of the HGTD //
  m_Conv->SetResolution(& m_Timing);
  m_Conv_l2->SetResolution(& m_Timing);


  m_Conv->SetcellReductionFactor(2); // Only usefull if m_granu == false
  m_Conv_l2->SetcellReductionFactor(2); // Only usefull if m_granu == false


  // Set an Array to determine which geometry correspond to which layer  
  if(m_Geo=="diagonale"){
    m_Det_l[0] = m_Det;
    m_Det_l[1] = m_Det;
    m_Det_l[2] = m_Det;
    m_Det_l[3] = m_Det;

    m_Conv_l[0] = m_Conv;
    m_Conv_l[1] = m_Conv;
    m_Conv_l[2] = m_Conv;
    m_Conv_l[3] = m_Conv;
  }

  if(m_Geo=="Helix_0" || m_Geo=="Helix_1"){
    m_Det_l[0] = m_Det;
    m_Det_l[1] = m_Det;
    m_Det_l[2] = m_Det;
    m_Det_l[3] = m_Det_l2;

    m_Conv_l[0] = m_Conv;
    m_Conv_l[1] = m_Conv;
    m_Conv_l[2] = m_Conv;
    m_Conv_l[3] = m_Conv_l2;
  }

  if(m_Geo=="Helix_2"){
    m_Det_l[0] = m_Det;
    m_Det_l[1] = m_Det;
    m_Det_l[2] = m_Det_l2;
    m_Det_l[3] = m_Det_l2;

    m_Conv_l[0] = m_Conv;
    m_Conv_l[1] = m_Conv;
    m_Conv_l[2] = m_Conv_l2;
    m_Conv_l[3] = m_Conv_l2;
  }

  m_hgtd_extrapolator = HGTDTrackExtrapolator( & m_extrapolator); // Set the tool use for the traxk extrapolation in the HGTD


  ////////////////////////////////////////////////
  // Set un a structure with all the useful tool//
  m_hgtdtool.hgtdID = m_hgtdID;
  for(int l=0;l<4;l++){
    m_hgtdtool.Det_l[l] = m_Det_l[l];
    m_hgtdtool.Conv_l[l] = m_Conv_l[l];
  }
  m_hgtdtool.Timing = & m_Timing;
  m_hgtdtool.hgtd_extrapolator = & m_hgtd_extrapolator;

  ////////////////////////////////////////////////

  if ( m_extrapolator.retrieve().isFailure() ) {
    msg(MSG::FATAL) << "Failed to retrieve tool " << m_extrapolator << endreq;
    return StatusCode::FAILURE;
  }

  return StatusCode::SUCCESS;
}

StatusCode HGTDHitAnalysis::finalize() {
  return StatusCode::SUCCESS;
}

StatusCode HGTDHitAnalysis::execute() {

  ATH_MSG_DEBUG( "In HGTDHitAnalysis::execute()" );
  m_nevent+=1; // increment the event number
  m_SampHit=0; // reinitialise the Sampling number


  ////////////////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////// Conversion of the Hits ////////////////////////////////////////


  const DataHandle<LArHitContainer> iter;
  if (evtStore()->retrieve(iter, m_hgtdHitsContName) == StatusCode::SUCCESS) { // Use that line for Digitisation data

    LArHitContainer::const_iterator hi;
    for (hi= iter->begin();hi!=iter->end();hi++){

      LArHit * hit;
      hit = new LArHit((*hi)->cellID(),(*hi)->energy(),(*hi)->time());
      hit->finalize();

      if ( hit->energy() >  0.02 ) { // Early cut to avoid computing the time of low energy hit
      
        // Store the hits with the proper Geometry
        if( ( m_hgtdID -> sampling(hit->cellID()) == 2 || m_hgtdID -> sampling(hit->cellID()) == 3 ) && m_Geo=="Helix_2" ){
          m_Conv_l2->TimeBin(hit, m_hgtdID); // We take all the hits in layer 3 and 4 (second cooling plate layer) and store them in timebins
        }
        
        if( m_hgtdID -> sampling(hit->cellID()) == 3 && (m_Geo=="Helix_0" || m_Geo=="Helix_1") ){
          m_Conv_l2->TimeBin(hit, m_hgtdID); // We take all the hits in layer 4 and store them in timebins
        }

        if( (m_hgtdID -> sampling(hit->cellID()) == 0 || m_hgtdID -> sampling(hit->cellID()) == 1) && m_Geo=="Helix_2"  ){
            m_Conv->TimeBin(hit, m_hgtdID); // We take all the hits in layer 1 and 2 (first cooling plate) and store them in timebins
        }
        
        if( m_hgtdID -> sampling(hit->cellID()) != 3 && (m_Geo=="Helix_0" || m_Geo=="Helix_1")  ){
            m_Conv->TimeBin(hit, m_hgtdID); // We take all the hits in layer 1, 2 and 3 and store them in timebins
        }  
          
      }
    }
    std::string outputCollName = "LArHitHGTDGranularity";
    SG::WriteHandle<LArHitContainer> LArHitColl(outputCollName);
    LArHitColl = CxxUtils::make_unique<LArHitContainer>(outputCollName);
    LArHitContainer * hitContainer = &*LArHitColl;
    m_Conv->Pulse(hitContainer, m_hgtdID); // We modify the hits in the time bin then return the collection
    m_Conv_l2->Pulse(hitContainer, m_hgtdID); // We modify the hits in the time bin then return the collection

  }

  m_xTruthEventContainer = NULL;
  CHECK( evtStore()->retrieve( m_xTruthEventContainer, "TruthEvents"));

  m_trackCont = NULL;
  if ( ! evtStore()->retrieve(m_trackCont,"InDetTrackParticles").isSuccess() ) {
    ATH_MSG_ERROR ("Failed to retrieve InDetTrackParticles collection. ");
    return StatusCode::SUCCESS;
  }

  ////////////////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////////////////////

  Float_t emax[4] = {-1., -1., -1., -1.};
  Float_t Rmax[4] = {-1., -1., -1., -1.};
  Float_t tmax[4] = {-1., -1., -1., -1.};
  int nCells[4] = {0, 0, 0, 0};

  /////////////////////////////////////////////////////////////////////////////
  ////////////////////////////// Analysis //////////////////////////////////////

  double TruthVert_z=0;
  double TruthVert_x=0;
  double TruthVert_y=0;
  double TruthVert_t=0;
  xAOD::TruthEventContainer::const_iterator itr;
  // Access the Truth Signal Vertex information //
  for(itr = m_xTruthEventContainer->begin(); itr!=m_xTruthEventContainer->end(); ++itr) {
    const xAOD::TruthVertex* vert = (*itr)->signalProcessVertex();
    TruthVert_z=vert->z();
    TruthVert_x=vert->x();
    TruthVert_y=vert->y();
    TruthVert_t=vert->t()/0.299792458;;
  }

  if (evtStore()->retrieve(iter,"LArHitHGTDGranularity")==StatusCode::SUCCESS) {

    LArHitContainer::const_iterator hi;
    for (hi= iter->begin();hi!=iter->end();hi++){

      // Conversion of the ID of the hits into position
      m_Conv_l[m_hgtdID -> sampling((*hi)->cellID())]->id_to_pos(m_hgtdID->x_index( (*hi)->cellID()), m_hgtdID->y_index( (*hi)->cellID()), m_coordx, m_coordy);
      m_coordr = sqrt(m_coordx*m_coordx + m_coordy*m_coordy);
      m_nsamp = m_hgtdID -> sampling((*hi)->cellID());
      m_energy = (*hi)->energy();
      m_zposneg  = m_hgtdID -> barrel_ec((*hi)->cellID());
      m_coordz = m_zposneg*m_Zpos[m_nsamp];
      m_time = (*hi)->time();

      m_treco = m_time + 11.8 - sqrt( (m_coordx-TruthVert_x)*(m_coordx-TruthVert_x)+(m_coordy-TruthVert_y)*(m_coordy-TruthVert_y)+(m_coordz-TruthVert_z)*(m_coordz-TruthVert_z) )/299.792458 - TruthVert_t/1000;

      m_granularity =  m_Conv_l[m_hgtdID -> sampling((*hi)->cellID())]->granularity_id(m_hgtdID->x_index((*hi)->cellID()),m_hgtdID->y_index((*hi)->cellID()));

      if (m_treeHit) m_treeHit->Fill(); // This tree is fill with one entry for each hit

      if ( (*hi)->energy() >  0.02 ) {
	nCells[m_nsamp]++;
      }

      if ( (*hi)->energy() > emax[m_nsamp] /*&& (*hi)->time() < 2. */) {
        emax[m_nsamp] = (*hi)->energy();
        Rmax[m_nsamp] = m_coordr;
        tmax[m_nsamp] = (*hi)->time();
      }
    } //end of the loop on all the energy deposit

    m_nCells0 = nCells[0];
    m_nCells1 = nCells[1];
    m_nCells2 = nCells[2];
    m_nCells3 = nCells[3];
    m_emax0 = emax[0];
    m_emax1 = emax[1];
    m_emax2 = emax[2];
    m_emax3 = emax[3];
    m_Rmax0 = Rmax[0];
    m_Rmax1 = Rmax[1];
    m_Rmax2 = Rmax[2];
    m_Rmax3 = Rmax[3];
    m_tmax0 = tmax[0];
    m_tmax1 = tmax[1];
    m_tmax2 = tmax[2];
    m_tmax3 = tmax[3];

    // Calculation of the number of Sampling hit
    if(m_emax0>0.02)
      m_SampHit+=1;
    if(m_emax1>0.02)
      m_SampHit+=1;
    if(m_emax2>0.02)
      m_SampHit+=1;
    if(m_emax3>0.02)
      m_SampHit+=1;
    if (m_tree) m_tree->Fill();

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////

    for( xAOD::TrackParticleContainer::const_iterator track = m_trackCont->begin(); track!= m_trackCont->end(); ++track ) {

      m_track_eta = (*track)->eta();
      m_track_Pt = (*track)->pt();
      m_track_R = tan(2*atan(exp(-1*(*track)->eta())))*m_Zpos[0];

      if(m_track_R>120 && m_track_R<600){

        HGTDTrack hgtd_track = HGTDTrack(m_hgtdtool);
        hgtd_track.HitAssociation(*track, iter);
        m_track_time = hgtd_track.GetTrackTime();
        if( hgtd_track.GetTrackTime() != -5 ){
          m_track_time_res = hgtd_track.GetTrackTime() - TruthVert_t/1000;
        }
        else {
          m_track_time_res = -5;
        }

        m_track_hit = hgtd_track.GetTrackHitsNumber();

        if (m_treeTrack) m_treeTrack->Fill();

      }

    }
  }

  ////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////

  return StatusCode::SUCCESS;
}
