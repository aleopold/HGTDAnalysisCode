#include "HGTDHitAnalysis/HGTDTrack.h"

HGTDTrack::HGTDTrack(){}

HGTDTrack::HGTDTrack(const HGTD_TOOL hgtdtool) :
  m_hgtdtool(hgtdtool){}


void HGTDTrack::SetHGTDTOOL(const HGTD_TOOL hgtdtool){
  m_hgtdtool = hgtdtool;
}

void HGTDTrack::HitAssociation(const xAOD::TrackParticle* trackParticle, const DataHandle<LArHitContainer> & iter){

  m_trackParticle = trackParticle;

  for(int s=0; s<4;s++){
    TVector3 vt1;
    TVector3 vpt1;
    double dist_close = 5;
    double time_close = -5;
    LArHit hit_close;

    if(m_hgtdtool.hgtd_extrapolator->Track_Extrapolation(trackParticle, vt1, vpt1, s)){

      LArHitContainer::const_iterator hi;
      for (hi= iter->begin();hi!=iter->end();hi++){
        if(m_hgtdtool.hgtdID->sampling((*hi)->cellID()) == s  &&  m_hgtdtool.hgtdID->barrel_ec((*hi)->cellID())*vt1.Z()>0 ){
          float x_h=0;
          float y_h=0;
          m_hgtdtool.Conv_l[s]->id_to_pos(m_hgtdtool.hgtdID->x_index( (*hi)->cellID()), m_hgtdtool.hgtdID->y_index( (*hi)->cellID()), x_h, y_h);

          if( sqrt( (x_h-vt1.X())*(x_h-vt1.X()) + (y_h-vt1.Y())*(y_h-vt1.Y()) ) < dist_close ){
            if(this->Fill_Factor(vt1.X(), vt1.Y(), s)){
              dist_close = sqrt( (x_h-vt1.X())*(x_h-vt1.X()) + (y_h-vt1.Y())*(y_h-vt1.Y()) );
              time_close = (*hi)->time() + 11.8 - (sqrt(x_h*x_h+y_h*y_h+(vt1.Z()-m_trackParticle->z0())*(vt1.Z()-m_trackParticle->z0()))/299.79245);
              hit_close = (*(*hi));
            }
          }
        }
      }
      if(time_close != -5){
        if(m_TrackTime == -5){
          m_TrackTime=0;
        }
        m_TrackTime += time_close;
        m_TrackHitsNumber++;
        m_TrackHits.push_back(hit_close);
      }
    }
  }
  if(m_TrackHitsNumber>0){
    m_TrackTime = m_TrackTime/m_TrackHitsNumber;
  }
}


const xAOD::TrackParticle* HGTDTrack::GetTrack(){
  return m_trackParticle;
}

std::vector<LArHit> HGTDTrack::GetTrackHits(){
  return m_TrackHits;
}

double HGTDTrack::GetTrackTime(){
  return m_TrackTime;
}

int HGTDTrack::GetTrackHitsNumber(){
  return m_TrackHitsNumber;
}

bool HGTDTrack::Fill_Factor(float x, float y, int layer){

  double xc = x;
  double yc = y;

  float x_cellsec=0;
  float y_cellsec=0;

  m_hgtdtool.Conv_l[layer]->pos_Cell(x, y, x_cellsec, y_cellsec);
  m_hgtdtool.Det_l[layer]->symmetry(xc,yc,0);

  HGTDCalculatorCell Cell = m_hgtdtool.Det_l[layer]->CellXY(xc,yc);
  if(Cell.is_connected(xc,yc)){

    if(fabs(x-x_cellsec)<(0.475) && fabs(y-y_cellsec)<(0.475) ){
      return true;
    }
  }
  return false;
}

bool HGTDTrack::TimingCompatible(HGTDTrack Track){
  TVector3 vt1;
  TVector3 vpt1;
  TVector3 vt2;
  TVector3 vpt2;

  m_hgtdtool.hgtd_extrapolator->Track_Extrapolation(m_trackParticle, vt1, vpt1, 0);
  m_hgtdtool.hgtd_extrapolator->Track_Extrapolation(Track.GetTrack(), vt2, vpt2, 0);

  double R1 = sqrt( vt1.X()*vt1.X() + vt1.Y()*vt1.Y() );
  double R2 = sqrt( vt2.X()*vt2.X() + vt2.Y()*vt2.Y() );

  double res1 = m_hgtdtool.Timing->TimingResolutionPerHit4R(R1);
  res1 = 0.001 * sqrt(res1*res1 - m_hgtdtool.Timing->ElectronicsResolution()*m_hgtdtool.Timing->ElectronicsResolution());

  double res2 = m_hgtdtool.Timing->TimingResolutionPerHit4R(R2);
  res2 = 0.001 * sqrt(res2*res2 - m_hgtdtool.Timing->ElectronicsResolution()*m_hgtdtool.Timing->ElectronicsResolution());

  double res = sqrt( res1*res1 + res2*res2 );

  if( fabs( Track.GetTrackTime() - m_TrackTime ) <= 2*res ){
    return true;
  }

  return false;

}
