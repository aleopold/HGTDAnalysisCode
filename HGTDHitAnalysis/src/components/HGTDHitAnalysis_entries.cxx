#include "HGTDHitAnalysis/HGTDHitAnalysis.h"
#include "GaudiKernel/DeclareFactoryEntries.h"

DECLARE_ALGORITHM_FACTORY( HGTDHitAnalysis )

DECLARE_FACTORY_ENTRIES( HitAnalysis ) {
  DECLARE_ALGORITHM( HGTDHitAnalysis )
}

