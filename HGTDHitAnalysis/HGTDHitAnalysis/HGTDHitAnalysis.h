#ifndef HGTD_HIT_ANALYSIS_H
#define HGTD_HIT_ANALYSIS_H

//#include "GaudiKernel/ToolHandle.h"
//#include "GaudiKernel/Algorithm.h"
//#include "GaudiKernel/ObjectVector.h"
//#include "CLHEP/Units/SystemOfUnits.h"

#include "AthenaBaseComps/AthAlgorithm.h"
#include "LArSimEvent/LArHitContainer.h"

#include "HGTDReadoutSim/HGTDCalculator.h"
#include "HGTDReadoutSim/HGTDHitConversion.h"

#include "xAODTruth/TruthEventContainer.h"

#include "TString.h"
#include "TH1.h"

#include "GaudiKernel/ToolHandle.h"
#include "TrkExInterfaces/IExtrapolator.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/TrackParticle.h"
#include "xAODTruth/TruthEventContainer.h"


#include "HGTDReadoutSim/HGTDTool.h"

class HGTD_ID;
class TileID;
class TileDetDescrManager;
class TTree;
class ITHistSvc;

class HGTDHitAnalysis : public AthAlgorithm {

 public:

   HGTDHitAnalysis(const std::string& name, ISvcLocator* pSvcLocator);
   ~HGTDHitAnalysis();

   virtual StatusCode initialize();
   virtual StatusCode finalize();
   virtual StatusCode execute();

 private:
   // Geo for layer 1 //
   HGTDCalculator * m_Det;
   HGTDHitConversion * m_Conv;

   // Geo for layer 2 //
   HGTDCalculator * m_Det_l2;
   HGTDHitConversion * m_Conv_l2;

   // Geo associated to each layer //
   HGTDCalculator * m_Det_l[4];
   HGTDHitConversion * m_Conv_l[4];

   bool m_pulse;
   bool m_granu;
   bool m_overlap;
   std::string m_Geo;

   Double_t m_Zpos[4] = {3516.93,3525.12,3533.33,3541.52};

   // Access the truth event //
   const xAOD::TruthEventContainer * m_xTruthEventContainer = NULL;
   const xAOD::TrackParticleContainer * m_trackCont = NULL;

   TTree * m_tree; // Tree containing informations on each event
   Float_t m_granularity; // granularity for this event : 0 -> 0.5*0.5 ; 1 -> function of the position
   Float_t m_nCells0; // number of cell hit in the first sampling
   Float_t m_nCells1; // number of cell hit in the second sampling
   Float_t m_nCells2; // number of cell hit in the third sampling
   Float_t m_nCells3; // number of cell hit in the last sampling
   Float_t m_emax0; // maximum energy deposit in the first sampling
   Float_t m_emax1; // maximum energy deposit in the second sampling
   Float_t m_emax2; // maximum energy deposit in the third sampling
   Float_t m_emax3; // maximum energy deposit in the last sampling
   Float_t m_Rmax0; // radius of the hit in the first sampling
   Float_t m_Rmax1; // radius of the hit in the second sampling
   Float_t m_Rmax2; // radius of the hit in the third sampling
   Float_t m_Rmax3; // radius of the hit in the last sampling
   Float_t m_tmax0; // time of the hit in the first sampling
   Float_t m_tmax1; // time of the hit in the second sampling
   Float_t m_tmax2; // time of the hit in the third sampling
   Float_t m_tmax3; // time of the hit in the last sampling
   Float_t m_SampHit; // number of sampling hit in this event

   std::string m_ntupleFileName;
   std::string m_ntupleDirName;
   std::string m_ntupleTreeName;
   std::string m_hgtdHitsContName;

   TTree * m_treeHit; // Tree containing informations on each hit
   Float_t m_coordx; // coordinate X of the hit
   Float_t m_coordy; // coordinate Y of the hit
   double m_coordz; // coordinate Z of the hit
   int m_nsamp; // sampling of the hit
   Float_t m_coordr; // coordinate R of the hit
   Float_t m_energy; // energy deposit of the hit
   Float_t m_time; // time of the hit
   Float_t m_treco; // time of the hit after correction of t_vertex and tof
   Float_t m_zposneg; // +1 for one of the EndCap, -1 for the other

   TTree * m_treeTrack; // Tree containing informations on each event
   Float_t m_track_eta; // eta of the track
   Float_t m_track_R; // eta of the track
   Float_t m_track_Pt; // Pt of the track
   Float_t m_track_time; // time of the track
   Float_t m_track_time_res; // time resolution of the track
   int m_track_hit; // number of hits associated with the track


   ITHistSvc * m_thistSvc;

   const TileID * m_tileID;
   const TileDetDescrManager * m_tileMgr;

   ToolHandle<Trk::IExtrapolator> m_extrapolator;

   Float_t m_nevent; // number of the event

   const HGTD_ID * m_hgtdID;


   HGTDTimingResolution m_Timing;

   HGTDTrackExtrapolator m_hgtd_extrapolator;

   HGTD_TOOL m_hgtdtool;

};

#endif // HGTD_HIT_ANALYSIS_H
