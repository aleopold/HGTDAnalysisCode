#ifndef HGTD_TRACK_H
#define HGTD_TRACK_H

#include "AthenaBaseComps/AthAlgorithm.h"
#include "LArSimEvent/LArHitContainer.h"

#include "HGTDReadoutSim/HGTDCalculator.h"
#include "HGTDReadoutSim/HGTDHitConversion.h"

#include "xAODTruth/TruthEventContainer.h"

#include "GaudiKernel/ToolHandle.h"
#include "TrkExInterfaces/IExtrapolator.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/TrackParticle.h"

#include "HGTDReadoutSim/HGTDTool.h"

#include <vector>

/**
* @file HGTDHitAnalysis/HGTDTrack.h
* @author Corentin Allaire
* @date January 2018
* @brief Object corresponding to the association of the HGTD timing information to a ITk track
*/

class HGTDTrack{

  /**
  * @brief Object corresponding to the association of the HGTD timing information to a ITk track.
  *
  * To each track a time and multiple HGTD hits are associated.
  */

public:
  /** Constructor 0 */
  HGTDTrack();

  /** Constructor A */
  HGTDTrack(const HGTD_TOOL hgtdtool);

  /** Set the HGTDTool (structure contining usefull tools) */
  void SetHGTDTOOL(const HGTD_TOOL hgtdtool);

  /** Associate HGTD hits to the ITk track */
  void HitAssociation(const xAOD::TrackParticle* trackParticle, const DataHandle<LArHitContainer> & iter);

  /** Return the ITk track */
  const xAOD::TrackParticle* GetTrack();

  /** Return a vector of HGTD hits associated with the ITk track */
  std::vector<LArHit> GetTrackHits();

  /** Return the time of the ITk track */
  double GetTrackTime();

  /** Return the number of associated HGTD hits */
  int GetTrackHitsNumber();

  /** Return if the position is within the active area of the pad */
  bool Fill_Factor(float x, float y, int layer);

  /** Return if the time of "Track" is compatible with the time of this HGTDTrack */
  bool TimingCompatible(HGTDTrack Track);

private:

  /** HGTDTool : structure contining usefull tools */
  HGTD_TOOL m_hgtdtool;

  /** ITk Track */
  const xAOD::TrackParticle* m_trackParticle;

  /** Vector of HGTD hits associated with the ITk track */
  std::vector<LArHit> m_TrackHits;

  /** Time of the ITk track */
  double m_TrackTime = -5;

  /** Number of associated HGTD hits */
  int m_TrackHitsNumber = 0;

};

#endif // HGTD_TRACK_H
