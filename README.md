# hgtd-simulation

## Synopsis

This project contains packages for emulating the granularity/geometry and effects of detector signal reconstruction for the HGTD. It also contains an example algorithm that uses these tools and performs some analysis on the resulting hits.

## List of packages

- HGTDReadoutSim
- HGTDHitAnalysis

## Installation instructions

```
setupATLAS
asetup 20.20.10.2,here
git clone https://:@gitlab.cern.ch:8443/atlas-hgtd/hgtd-simulation.git
cmt find_packages && cmt compile
# cd to where you want to run, then link to these files that are needed to be present in the run dir
ln -s $TestArea/hgtd-simulation/HGTDReadoutSim/InDetIdDictFiles/ .
```

To try a test run of the HGTDHitAnalysis algorithm and produce an ntuple with an output tree, just do:

```
athena.py hgtd-simulation/HGTDHitAnalysis/share/HGTDHitAnalysis_topOptions.py 2>&1 | tee test.log
```

## Additional info

### Name of the container with HGTD hits
In HITS files the container with the hits is called "LArHitHGTD", in RDOs/AODs it's called "HGTDDigitContainer_MC". The latter is the default assumed by HGTDHitAnalysis. (this is the default). 
